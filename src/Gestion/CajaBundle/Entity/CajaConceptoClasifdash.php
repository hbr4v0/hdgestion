<?php

namespace Gestion\CajaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * caja_concepto_clasifdash
 *
 * @ORM\Table(name="caja_concepto_clasifdash", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 */

class CajaConceptoClasifdash
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")

     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nivel", type="integer", nullable=false)
     */
    private $nivel;     
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="clasifdash", type="string", nullable=false)
     */
    private $clasifdash;    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoria
     *
     * @param string $clasifdash
     * @return string
     */
    public function setClasifdash($clasifdash)
    {
        $this->clasifdash = $clasifdash;

        return $this;
    }

    /**
     * Get clasifdash
     *
     * @return string 
     */
    public function getClasifdash()
    {
        return $this->clasifdash;
    }

    
 public function __toString()
    {
        return strval($this->id);
    }
    
}