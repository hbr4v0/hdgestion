<?php
namespace Gestion\TraspasosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Condition\ConditionBuilderInterface;

class filtroInfTrnfFechaToType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder->add('fecha_transfer','filter_date_range', array(
                'left_date_options' => array('label' => 'Desde:',
                                             'widget' => 'single_text',
                                             'data' => new \DateTime(date('Y-m-01'))),
                'right_date_options' => array('label' => 'Hasta:',
                		              'widget' => 'single_text',
                                              'data' => new \DateTime("now"))))
                ->add('op_id', 'filter_entity', array(
                    'class' => 'GestionTraspasosBundle:TransferOps',
                    'property' => 'operador',
                    'attr' => array('class' => 'form-control'),
                    'required' => 'true',
                    'empty_value'=>'Seleccione...',
                    'label' => 'Operador',
));
		
    }

    public function getName()
    {
        return 'filtro_traspasos';
    }
}