<?php
namespace Gestion\CajaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Condition\ConditionBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Gestion\CajaBundle\Entity\CajaConcepto;

class filtroBuscarCajasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $tipo = array(
            '1'    => 'Ingresos',
            '2'   => 'Egresos',
        );
        
    	$builder->add('fecha','filter_date_range', array(
                    'left_date_options' => array(
                    'label' => 'Desde:',
                    'widget' => 'single_text',
                    'data' => new \DateTime(date('Y-m-01'))),
                    'right_date_options' => array(
                    'label' => 'Hasta:',
                    'widget' => 'single_text',
                    'data' => new \DateTime("now"))))
                ->add('cajaconcepto_id', 'entity', array(
                    'class' => 'GestionCajaBundle:CajaConcepto',
                    'property' => 'concepto',
                    'empty_value'=>'TODOS',
                    'required' => 'false',
                    'empty_data'  => null,
                    'attr' => array('class' => 'form-control'),
                    'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                    ->where('c.tipo = 2')
                    ->orderBy('c.concepto', 'ASC');},))
                ->add('moneda_id', 'entity', array(
                    'class' => 'GestionCajaBundle:Moneda',
                    'property' => 'moneda',
                    'empty_value'=>'TODOS',
                    'required' => 'false',
                    'empty_data'  => null,
                    'attr' => array('class' => 'form-control'),
                    'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c');
                    },
))
                //->add('tipo', 'choice', array(
                //      'choices' => $tipo,
                //      'empty_value'=>'TODOS',
                //      'required' => 'false',
                //      'empty_data'  => null,
                //      'attr' => array('class' => 'form-control'),
                //        ))
            ;
                
                ;		
    }

    public function getName()
    {
        return 'filtro_cajas';
    }
}