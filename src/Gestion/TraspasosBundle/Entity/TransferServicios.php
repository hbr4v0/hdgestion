<?php

namespace Gestion\TraspasosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * transfer_servicios
 *
 * @ORM\Table(name="transfer_servicios", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 */

class TransferServicios
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="servicio", type="string", nullable=false)
     */
    private $servicio;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set servicio
     *
     * @param string $servicio
     *
     * @return TransferServicios
     */
    public function setServicio($servicio)
    {
        $this->servicio = $servicio;

        return $this;
    }

    /**
     * Get servicio
     *
     * @return string
     */
    public function getServicio()
    {
        return $this->servicio;
    }

 
    
    
    
    /**
    * @ORM\ManyToOne(targetEntity="Transfers", inversedBy="transfer")  
    * @ORM\JoinColumn(name="traspasotour_id", referencedColumnName="id")
    **/
    
    protected $transferservicios;
    
    
 public function __toString()
    {
        return strval($this->id);
    }
    
    
}

