<?php

namespace Gestion\CajaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * caja_concepto
 *
 * @ORM\Table(name="caja_concepto_categ", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 */

class CajaConceptoCateg
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")

     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo", type="integer", nullable=false)
     */
    private $tipo;    
    
    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=50, nullable=false)
     */
    private $categoria;

    /**
     * @var integer
     *
     * @ORM\Column(name="operacional", type="integer", nullable=false)
     */
    private $operacional;    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param integer $tipo
     * @return integer
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Set categoria
     *
     * @param string $categoria
     * @return string
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return string 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set categoria
     *
     * @param integer $operacional 
     * @return integer
     */
    public function setOperacional($operacional)
    {
        $this->operacional = $operacional;

        return $this;
    }

    /**
     * Get operacional
     *
     * @return integer
     */
    public function getOperacional()
    {
        return $this->operacional;
    }
    
 public function __toString()
    {
        return strval($this->id);
    }
    
}