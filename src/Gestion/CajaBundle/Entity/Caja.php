<?php

namespace Gestion\CajaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Caja
 *
 * @ORM\Table(name="caja")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Gestion\CajaBundle\Entity\CajaRepository")
 */

class Caja
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=false)
     */
    private $nombre;    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=50, nullable=false)
     */
    private $usuario;

    /**
     * @var integer
     *
     * @ORM\Column(name="monto", type="integer", nullable=false)
     */
    private $monto;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo", type="integer", nullable=false)
     */
    private $tipo;

     /**
     * @var integer
     *
     * @ORM\Column(name="cajaconcepto_id", type="integer", nullable=false)

     */
    private $cajaconcepto_id;

     /**
     * @var integer
     *
     * @ORM\Column(name="moneda_id", type="integer", nullable=false)

     */
    private $moneda_id;    
    
    /**
     * @var integer;
     *
     * @ORM\Column(name="nula", type="integer", nullable=false)
     */
    private $nula;

    /**
     * @var string;
     *
     * @ORM\Column(name="obs", type="string", nullable=true, length=255)
     */
    private $obs;    

    /**
     * @var integer
     *
     * @ORM\Column(name="hab", type="integer", nullable=true)
     */
    private $hab;    
     
    /**
     * @var integer
     *
     * @ORM\Column(name="doc", type="integer", nullable=true)
     */
    private $doc;       
    
    /**
     * @var string
     *
     * @ORM\Column(name="pax", type="string", length=50, nullable=true)
     */
    private $pax;    

        /**
     * @var time
     *
     * @ORM\Column(name="hora", type="time", nullable=false)
     */
    private $hora;    
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }

    /**
     * Get tipo
     *
     * @return integer
     */
    public function getTipo()
    {
    	return $this->tipo;
    }

    /**
     * Set tipo
     *
     * @param integer $tipo
     * @return integer
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    } 

    /**
     * Get usuario
     *
     * @return string
     */
    public function getUsuario()
    {
    	return $this->usuario;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return string
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    } 

    /**
     * Get monto
     *
     * @return integer
     */
    public function getmonto()
    {
    	return $this->monto;
    }

    /**
     * Set monto
     *
     * @param integer $monto
     * @return integer
     */
    public function setMonto($monto)
    {
    	$this->monto = $monto;
    
    	return $this;
    }   

    /**
     * Set fecha
     *
     * @param date $fecha
     * @return date
     */
    public function setFecha($fecha)
    {
    	$this->fecha = $fecha;
    
    	return $this;
    } 
    
    /**
     * Get fecha
     *
     * @return date
     */
    public function getFecha()
    {
    	return $this->fecha;
    }
    
    /**
     * Get nula
     *
     * @return integer
     */
    public function getNula()
    {
    	return $this->nula;
    }

    /**
     * Set nula
     *
     * @param integer $nula
     * @return integer
     */
    
    public function setNula($nula)
    {
    	$this->nula = $nula;
    
    	return $this;
    }   

    /**
     * Get cajaconcepto_id
     *
     * @return integer
     */
    public function getCajaconceptoId()
    {
    	return $this->cajaconcepto_id;
    }

    /**
     * Set cajaconcepto_id
     *
     * @param integer $cajaconcepto_id
     * @return integer
     */
    
    public function setCajaconceptoId($cajaconcepto_id)
    {
    	$this->cajaconcepto_id = $cajaconcepto_id;
    
    	return $this;
    }   
    /**
     * Get moneda_id
     *
     * @return integer
     */
    public function getMonedaId()
    {
    	return $this->moneda_id;
    }

    /**
     * Set moneda_id
     *
     * @param integer $moneda_id
     * @return integer
     */
    
    public function setMonedaId($moneda_id)
    {
    	$this->moneda_id = $moneda_id;
    
    	return $this;
    }   
   /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
    	return $this->nombre;
    }

    /**
     * Set usuario
     *
     * @param string $nombre
     * @return string
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    } 
 
    /**
     * Get hab
     *
     * @return integer
     */
    public function getHab()
    {
    	return $this->hab;
    }

    /**
     * Set hab
     *
     * @param integer $hab
     * @return integer
     */
    public function setHab($hab)
    {
        $this->hab = $hab;

        return $this;
    }    
 
     /**
     * Get doc
     *
     * @return integer
     */
    public function getDoc()
    {
    	return $this->doc;
    }

    /**
     * Set doc
     *
     * @param integer $doc
     * @return integer
     */
    public function setDoc($doc)
    {
        $this->doc = $doc;

        return $this;
    }  
    
    /**
     * Get pax
     *
     * @return string
     */
    public function getPax()
    {
    	return $this->pax;
    }

    /**
     * Set pax
     *
     * @param string $pax
     * @return string
     */
    public function setPax($pax)
    {
        $this->pax = $pax;

        return $this;
    }   
    
    /**
     * Get obs
     *
     * @return string
     */
    public function getObs()
    {
    	return $this->obs;
    }

    /**
     * Set obs
     *
     * @param string $obs
     * @return string
     */
    public function setObs($obs)
    {
        $this->obs = $obs;

        return $this;
    }   

     /**
     * Get hora
     *
     * @return time
     */
    public function getHora()
    {
    	return $this->hora;
    }

    /**
     * Set hora
     *
     * @param time $hora
     * @return time
     */
    public function setHora($hora)
    {
        $this->hora = $hora;

        return $this;
    }      
    
    
    /**
     * @ORM\OneToMany(targetEntity="CajaConcepto", mappedBy="caja")
     * @ORM\OneToMany(targetEntity="Moneda", mappedBy="caja")
     */
    private $cajas;
    
    public function __construct()
        
    {
        $this->cajas = new ArrayCollection();
    }
}
