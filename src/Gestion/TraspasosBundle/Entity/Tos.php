<?php

namespace Gestion\TraspasosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * tos
 *
 * @ORM\Table(name="tos", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 */

class Tos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="operador", type="string", nullable=false)
     */
    private $operador;

    /**
     * @var integer
     * @ORM\Column(name="tipo", type="integer", nullable=false)
     */
    private $tipo;    
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set operador
     *
     * @param string $operador
     *
     * @return Tos
     */
    public function setOperador($operador)
    {
        $this->operador = $operador;

        return $this;
    }

    /**
     * Get operador
     *
     * @return string
     */
    public function getOperador()
    {
        return $this->operador;
    }

    
    /**
     * Set tipo
     *
     * @param integer $tipo
     *
     * @return Tos
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer
     */
    public function getTipo()
    {
        return $this->tipo;
    }    
    
    
    
    
    /**
    * @ORM\ManyToOne(targetEntity="Traspasos", inversedBy="traspasos")  
    * @ORM\JoinColumn(name="to_id", referencedColumnName="id")
    */
    protected $traspaso;
    

    
 public function __toString()
    {
        return strval($this->id);
    }    
    
}

