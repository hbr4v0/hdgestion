<?php

namespace Gestion\TraspasosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * transfer_ops
 *
 * @ORM\Table(name="transfer_ops", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 */

class TransferOps
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="operador", type="string", nullable=false)
     */
    private $operador;
        
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set operador
     *
     * @param string $operador
     *
     * @return TransferOps
     */
    public function setOperador($operador)
    {
        $this->operador = $operador;

        return $this;
    }

    /**
     * Get operador
     *
     * @return string
     */
    public function getOperador()
    {
        return $this->operador;
    }

    /**
    * @ORM\ManyToOne(targetEntity="Transfers", inversedBy="transfers")  
    * @ORM\JoinColumn(name="op_id", referencedColumnName="id")
    */
    protected $transfers;
    

    
 public function __toString()
    {
        return strval($this->id);
    }    
    
}

