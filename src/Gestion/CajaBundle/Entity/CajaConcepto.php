<?php

namespace Gestion\CajaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * caja_concepto
 *
 * @ORM\Table(name="caja_concepto", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 */

class CajaConcepto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")

     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo", type="integer", nullable=false)
     */
    private $tipo;    

        /**
     * @var integer
     *
     * @ORM\Column(name="cat_id", type="integer", nullable=false)
     */
    private $cat_id;  

    /**
     * @var integer
     *
     * @ORM\Column(name="nulo", type="integer", nullable=false)
     */
    private $nulo;  
    
    /**
     * @var string
     *
     * @ORM\Column(name="concepto", type="string", length=50, nullable=false)

     */
    private $concepto;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set concepto
     *
     * @param string $concepto
     * @return string
     */
    public function setConcepto($concepto)
    {
        $this->concepto = $concepto;

        return $this;
    }

    /**
     * Get concepto
     *
     * @return string 
     */
    public function getConcepto()
    {
        return $this->concepto;
    }
    
    /**
     * Set nulo
     *
     * @param integer $nulo
     * @return integer
     */
    public function setNulo($nulo)
    {
        $this->nulo = $nulo;

        return $this;
    }

    /**
     * Get nulo
     *
     * @return integer
     */
    public function getNulo()
    {
        return $this->nulo;
    }


    
    /**
     * Set cat_id
     *
     * @param integer $cat_id
     * @return integer
     */
    public function setCatId($cat_id)
    {
        $this->cat_id = $cat_id;

        return $this;
    }

    /**
     * Get cat_id
     *
     * @return integer
     */
    public function getCatId()
    {
        return $this->cat_id;
    }


    
    /**
    * @ORM\ManyToOne(targetEntity="Caja", inversedBy="cajas")  
    * @ORM\JoinColumn(name="cajaconcepto_id", referencedColumnName="id")
    */
    protected $caja;
    
 public function __toString()
    {
        return strval($this->id);
    }
    
}