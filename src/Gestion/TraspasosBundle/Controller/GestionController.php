<?php

namespace Gestion\TraspasosBundle\Controller;

use Gestion\TraspasosBundle\Entity\Traspasos;
use Gestion\TraspasosBundle\Entity\Tos;
use Gestion\TraspasosBundle\Entity\TraspasosTours;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ob\HighchartsBundle\Highcharts\Highchart;

class GestionController extends Controller
{
    public function indexgestionAction()   
    {
        $hoy = new \DateTime("now");
        $ayer = new \DateTime("now");
        $ayer->modify('-1 day');
        $m = new \DateTime("now");
        $m->modify('+1 day');
        $manana = $m->format("Y-m-d");
        $m->modify('+1 day');
        $pasado = $m->format("Y-m-d");
        
        $em = $this->getDoctrine()->getManager();    
        
        //RESUMEN MENSUAL
        $mes=date("m");
        $anio=date("Y");
        $dias_mes= date("d",(mktime(0,0,0,$mes+1,1,$anio)-1));
        $dias=intval($dias_mes);
        $hasta= $anio . "-" . $mes . "-" . $dias ;
        $desde= date('Y-m-01'); // first day of this month
        
        $query4 = $em->createQuery(
        "SELECT o.operador, SUM(t.costo) AS costo, SUM(t.cobrado) AS cobrado   
         FROM GestionTraspasosBundle:Traspasos t 
         inner JOIN GestionTraspasosBundle:Tos o
         with t.to_id = o.id
         WHERE (t.fecha_traspaso BETWEEN  '" . $desde . "' AND '" . $hasta . "') AND t.nula=0  
         GROUP BY o.operador");
        $tour_resumen_mes = $query4->getResult();    
    
        
        $gra_resumen_tours_mes_data= array_column($tour_resumen_mes, 'q1') ;

        // RESUMEN QUINCENAL VENTAS TOTALES 6 MESES
        
        //Recupera TOs y los mete a arreglo
        $query = $em->createQuery(
        "SELECT o.operador, SUM(t.costo) AS costo, SUM(t.cobrado) AS cobrado   
         FROM GestionTraspasosBundle:Traspasos t 
         inner JOIN GestionTraspasosBundle:Tos o
         with t.to_id = o.id
         WHERE (t.fecha_traspaso BETWEEN  '" . $desde . "' AND '" . $hasta . "') AND t.nula=0  
         GROUP BY o.operador");
        $ret = $query->getResult();
        
        $qq=0;
                
        for ($i = 0; $i <= 11; $i++)
            {
                if ( $i > 0 ){
                $m->modify('-1 month');
                }
                
                $mes=$m->format("M") . "/" . $m->format("y");
                $nmes=$m->format("m");
                
                $fecha1_q1 = $m->format('Y') . "-" . $m->format('m') . "-01";
                $fecha2_q1 = $m->format('Y') . "-" . $m->format('m') . "-15";
                
                $fecha1_q2 = $m->format('Y') . "-" . $m->format('m') . "-16";

                $anio=date("Y");
                $dias_mes= date("d",(mktime(0,0,0,$nmes+1,1,$anio)-1));
                $dias=intval($dias_mes);
                
                $fecha2_q2 = $m->format('Y') . "-" . $m->format('m') . "-" . $dias;
                
                
                $q=$em->createQuery("SELECT Sum(t.costo) AS costo, Sum(t.cobrado) AS cobrado
                                    FROM GestionTraspasosBundle:Traspasos t 
                                    WHERE t.fecha_traspaso Between '". $fecha1_q1 . "' And '" . $fecha2_q1 . "' AND t.nula=0"
                                    );
                $result=$q->getResult();
                $q1= array_shift($result['0']);
 
                $q=$em->createQuery("SELECT Sum(t.costo) AS costo, Sum(t.cobrado) AS cobrado
                                    FROM GestionTraspasosBundle:Traspasos t 
                                    WHERE t.fecha_traspaso Between '". $fecha1_q2 . "' And '" . $fecha2_q2 . "' AND t.nula=0"
                                    );
                $result=$q->getResult();
                $q2= array_shift($result['0']);
                
                
                $q=$em->createQuery("SELECT Sum(t.cobrado) AS cobrado
                                    FROM GestionTraspasosBundle:Traspasos t 
                                    WHERE t.fecha_traspaso Between '". $fecha1_q1 . "' And '" . $fecha2_q2 . "' AND t.nula=0"
                                    );
                $result=$q->getResult();
                $cobrado= array_shift($result['0']);                
                
                //var_dump($cobrado);
                
                $q1=intval($q1);
                $q2=intval($q2);
                
                $qq=$q1+$q2;
                
                $cobrado=intval($cobrado);

                $ret[$i] = array('mes'=>$mes, 'q1'=>$q1, 'q2'=>$q2, 'total'=>$qq, 'cobrado'=>$cobrado);
            }

            // % Variacion
            
            $tot1 = $ret[0]['total'];
            $tot2 = $ret[1]['total'];
            $tot3 = $ret[2]['total'];
            $tot4 = $ret[3]['total'];
            $tot5 = $ret[4]['total'];
            $tot6 = $ret[5]['total'];

            
            if ($tot2 == '0'){
                $var1 = 100;
            }
            else {
                $var1 = (100-(($tot1*100)/$tot2))*-1;
            }
 
            if ($tot3 == '0'){
                $var2 = 0;
            }
            else {
                $var2 = (100-(($tot2*100)/$tot3))*-1;
            }
            
            if ($tot4 == '0'){
                $var3 = 0;
            }
            else {
                $var3 = (100-(($tot3*100)/$tot4))*-1;
            }
            
            if ($tot5 == '0'){
                $var4 = 0;
            }
            else {
                $var4 = (100-(($tot4*100)/$tot5))*-1;
            }           

            if ($tot6 == '0'){
                $var5 = 0;
            }
            else {
                $var5 = (100-(($tot5*100)/$tot6))*-1;
            }   
            
            $var6=0; 
            
            $ret[0]['variacion']=$var1;
            $ret[1]['variacion']=$var2;
            $ret[2]['variacion']=$var3;
            $ret[3]['variacion']=$var4;
            $ret[4]['variacion']=$var5;     
            $ret[5]['variacion']=$var6;     
            
        $tour_historico_vtas=$ret;
        unset($tour_historico_vtas[6]);
        unset($tour_historico_vtas[7]);
        unset($tour_historico_vtas[8]);
        unset($tour_historico_vtas[9]);
        unset($tour_historico_vtas[10]);
        unset($tour_historico_vtas[11]);
        
        $gra_historico_vtas_data1= array_column($ret, 'q1') ;
        $gra_historico_vtas_data2= array_column($ret, 'q2') ;
        $gra_historico_vtas_data3= array_column($ret, 'total') ;
        $gra_historico_vtas_data4= array_column($ret, 'cobrado') ;
        $gra_historico_vtas_cat= array_column($ret, 'mes') ;

        // GRAFICO VENTA TOURS
        $series = array(
        array("name" => "Costo","data" => $gra_historico_vtas_data3),
        array("name" => "Cobrado","data" => $gra_historico_vtas_data4),
        //array("name" => "Q2","data" => $gra_historico_vtas_data2),
        //array("name" => "Q1","data" => $gra_historico_vtas_data1)
        );
        $ob1 = new Highchart();
        $ob1->chart->renderTo('chart_1');  // The #id of the div where to render the chart
        $ob1->chart->type('line');
        $ob1->title->text('Historico Venta Mensual Tours');
        $ob1->subtitle->text('...');
        //$ob->xAxis->title(array('text'  => "7 últimos dias"));
        $ob1->yAxis->title(array('text'  => "..."));
        $ob1->xAxis->categories($gra_historico_vtas_cat);
        $ob1->plotOptions->line(array(
        //'allowPointSelect'  => true,
        //'stacking'    => 'normal',
        //'depth'     =>  '45',
        //'innerSize' => '100',
        //'dataLabels'    => array('enabled' => true),
        'numericsymbols'  => false
        ));
        $ob1->series($series);
        
        // ULTIMOS PAGOS TOURS
        $query4 = $em->createQuery(
        "SELECT t.id, t.pax, t.n, t.fecha_pago as fecha,t.hora_pago, t.formapago, t.cobrado  
         FROM GestionTraspasosBundle:Traspasos t 
         WHERE t.pagado = '1'
         ORDER BY t.fecha_pago DESC
         ")->setMaxResults(10);
        $ultimos_pagos = $query4->getResult();  
        
        //INFO TRANSFERS     
        
        //RESUMEN MENSUAL
        $mes=date("m");
        $anio=date("Y");
        $dias_mes= date("d",(mktime(0,0,0,$mes+1,1,$anio)-1));
        $dias=intval($dias_mes);
        $hasta= $anio . "-" . $mes . "-" . $dias ;
        $desde= date('Y-m-01'); // first day of this month
        
        $query8 = $em->createQuery(
        "SELECT o.operador, p.servicio, Sum(t.n) AS N
         FROM GestionTraspasosBundle:Transfers t 
         inner JOIN GestionTraspasosBundle:TransferOps o
         with t.op_id = o.id
         inner JOIN GestionTraspasosBundle:TransferServicios p
         with t.servicio_id = p.id
         WHERE t.fecha_transfer Between '" . $desde . "' AND '" . $hasta . "' AND t.nula=0
         GROUP BY o.operador, p.servicio, t.nula
         HAVING t.nula=0
         ORDER BY p.servicio");
        $trnf_resumen_mes = $query8->getResult();  
        
        // GRAFICO VENTA HISTORICA TRANSFER
        $m = new \DateTime("now");
        for ($i = 0; $i <= 11; $i++)
            {
                if ( $i > 0 ){
                    $m->modify('-1 month');
                }
                $mes=$m->format("M") . "/" . $m->format("y");
                $nmes=$m->format("m");
                
                $fecha1 = $m->format('Y') . "-" . $m->format('m') . "-01";
                $anio=date("Y");
                $dias_mes= date("d",(mktime(0,0,0,$nmes+1,1,$anio)-1));
                $dias=intval($dias_mes);
                
                $fecha2 = $m->format('Y') . "-" . $m->format('m') . "-" . $dias;

                $q=$em->createQuery("SELECT Sum(t.n) AS N
                                    FROM GestionTraspasosBundle:Transfers t 
                                    WHERE (t.fecha_transfer Between '" . $fecha1 . "' And '" . $fecha2 . "') AND (t.servicio_id=1) AND (t.nula=0)"
                                    );
                $result=$q->getResult();
                $trnfin= array_shift($result['0']);
 
                $q=$em->createQuery("SELECT Sum(t.n) AS N
                                    FROM GestionTraspasosBundle:Transfers t 
                                    WHERE (t.fecha_transfer Between '" . $fecha1 . "' And '" . $fecha2 . "') AND (t.servicio_id=2) AND (t.nula=0)"
                                    );
                $result=$q->getResult();
                $trnfout= array_shift($result['0']);
                
                $trnf=$trnfin + $trnfout;
                
                $ret_trnf[$i] = array('mes'=>$mes, 'trnfin'=>$trnfin, 'trnfout'=>$trnfout, 'total'=>$trnf);
            } 
        
        $gra_historico_transfer_data= array_column($ret_trnf, 'total') ;
        $gra_historico_transfer_cat= array_column($ret_trnf, 'mes') ;

        $series = array(
        //array("name" => "TOTAL","data" => $gra_historico_vtas_data3),
        array("name" => "Numero de Servicios","data" => $gra_historico_transfer_data)
        );
        $ob2 = new Highchart();
        $ob2->chart->renderTo('chart_2');  // The #id of the div where to render the chart
        $ob2->chart->type('bar');
        $ob2->title->text('Historico Ventas Transfer');
        $ob2->subtitle->text('...');
        //$ob->xAxis->title(array('text'  => "7 últimos dias"));
        $ob2->yAxis->title(array('text'  => "..."));
        $ob2->xAxis->categories($gra_historico_transfer_cat);
        $ob2->plotOptions->bar(array(
        'allowPointSelect'  => true,
        'stacking'    => 'normal',
        'dataLabels'    => array('enabled' => true),
        'numericsymbols'  => false
        ));
        $ob2->series($series);
        // FIN INFO TRANSFER

        return $this->render
                ('GestionTraspasosBundle:Default:index.gestion.html.twig', 
                array(
                    
                    'hoy'=>$hoy,
                    'manana'=>$manana,
                    'pasado'=>$pasado,
                    'tour_resumen_mes'=>$tour_resumen_mes,
                    'tour_historico_vtas'=>$tour_historico_vtas,
                    'chart_1'=>$ob1,
                    'chart_2'=>$ob2,
                    'trnf_resumen_mes'=>$trnf_resumen_mes,
                    'ultimos_pagos'=>$ultimos_pagos
                    )
                    );
    } 
}
