<?php

namespace Gestion\CajaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Gestion\CajaBundle\Entity\Caja;
use Gestion\CajaBundle\Entity\CajaRepository;
use Gestion\CajaBundle\Entity\CajaCierre;
use Gestion\CajaBundle\Entity\CajaCierreRepository;
use Gestion\CajaBundle\Entity\CajaConcepto;
use Gestion\CajaBundle\Controller\MiscController;
use Symfony\Component\HttpFoundation\Request;
use Ob\HighchartsBundle\Highcharts\Highchart;

class DefaultController extends Controller
{
    
    public function indexAction()
            
    {
        return $this->render('GestionCajaBundle:Default:index.html.twig');
    }

    public function addcajaAction()
            
    {
            $caja = new Caja();
            $concepto = new CajaConcepto();
            $CajaCierre = new CajaCierre();

            $em = $this->getDoctrine()->getManager();
            $cierre = $em->getRepository('GestionCajaBundle:CajaCierre')
                         ->ultimoCierre();
            $cierre= array_shift($cierre['0']);
            
            //$str = '2011-08-13 13:25:00';
            $objCierre = \DateTime::createFromFormat('Y-m-d', $cierre);
            //$objCierre->modify('+1 day');
            
            $cierre_usuario = $em->getRepository('GestionCajaBundle:CajaCierre')
                         ->getUsuarioCierre($cierre);

            $cierre_fecha = $em->getRepository('GestionCajaBundle:CajaCierre')
                         ->getFechaCierre($cierre);

            $cierre_saldo_peso = $em->getRepository('GestionCajaBundle:CajaCierre')
                         ->getSaldoCierrePeso($cierre);

            $cierre_saldo_dolar = $em->getRepository('GestionCajaBundle:CajaCierre')
                         ->getSaldoCierreDolar($cierre);
            
            $cierre_fecha= array_shift($cierre_fecha['0']);
            $cierre_usuario= array_shift($cierre_usuario['0']);

            $cierre_saldo_peso= array_shift($cierre_saldo_peso['0']);
            $cierre_saldo_dolar= array_shift($cierre_saldo_dolar['0']);

            
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
            'SELECT c.id, c.fecha, c.hora, c.monto, c.nombre, c.tipo, o.concepto, c.nula, m.moneda,m.id AS idmoneda, c.doc, c.hab, c.pax, c.obs 
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:Moneda m
             with c.moneda_id = m.id
             WHERE c.fecha > :cierre
             ORDER BY c.id DESC'
            )->setParameter('cierre', $cierre);
            
            $movs = $query->getResult();
            
            $em = $this->getDoctrine()->getManager();
            $query2 = $em->createQuery(
            'SELECT c.cierre , c.saldo, c.hora, c.usuario
             FROM GestionCajaBundle:CajaCierre c 
             WHERE c.moneda_id = :moneda_id 
             ORDER BY c.fecha DESC')
            ->setParameter('moneda_id', '1')
            ->setMaxResults(10);
            
            $cierres_clp = $query2->getResult();
            
            $em = $this->getDoctrine()->getManager();
            $query3 = $em->createQuery(
            'SELECT c.cierre , c.saldo, c.hora, c.usuario
             FROM GestionCajaBundle:CajaCierre c 
             WHERE c.moneda_id = :moneda_id 
             ORDER BY c.fecha DESC')
            ->setParameter('moneda_id', '2')
            ->setMaxResults(10);
            
            $cierres_usd = $query3->getResult();           
            
            $cierres = array();
            
            for ($i = 0; $i <= 5; $i++)
            {
                $cierres[$i] = array('fecha'=>$cierres_clp[$i]['cierre'], 'saldo_clp'=>$cierres_clp[$i]['saldo'],'saldo_usd'=>$cierres_usd[$i]['saldo'],'hora'=>$cierres_clp[$i]['hora'],'usuario'=>$cierres_clp[$i]['usuario']);
            }
            //Datos NAVBAR
            $saldo_actual_clp = $em->getRepository('GestionCajaBundle:CajaCierre')
                         ->getSaldoActual(1);
            $saldo_actual_usd = $em->getRepository('GestionCajaBundle:CajaCierre')
                         ->getSaldoActual(2);  
            $nmes = date('m');
            //var_dump($nmes);
            $tours_por_cobrar = $em->getRepository('GestionTraspasosBundle:Traspasos')
                         ->getToursPorCobrar($nmes); 
            
            return $this->render
                    ('GestionCajaBundle:Default:index.html.twig', 
                    array(
                        'cierre'=>$cierre,
                        'cierre_fecha'=>$cierre_fecha,
                        'cierre_usuario'=>$cierre_usuario,
                        'cierre_saldo_peso'=>$cierre_saldo_peso,
                        'cierre_saldo_dolar'=>$cierre_saldo_dolar,
                        'movs'=>$movs,
                        'cierres'=>$cierres,
                        'saldo_actual_clp'=>$saldo_actual_clp,
                        'saldo_actual_usd'=>$saldo_actual_usd,
                        'saldo_actual_clp' => $saldo_actual_clp,
                        'saldo_actual_usd' => $saldo_actual_usd,
                        'tours_por_cobrar' => $tours_por_cobrar,
                        'mes_actual' => strtoupper(date('M')),
                        //'valor_usd'=>$valor_dolar,'
                        //'valor_euro'=>$valor_euro
                    )
                    );
    }
    
    
    public function addegresoAction(Request $request)
            
    {
            // VALOR DOLAR 
            #$misc = new MiscController();
            #$valor_dolar= $misc->obtieneValorDolar();
            #$valor_euro= $misc->obtieneValorEuro();
            $caja = new Caja();
           
            $caja->setFecha(new \DateTime("now"));
            
            $form = $this->createFormBuilder($caja)
            ->add('fecha', 'genemu_jquerydate',array('widget'  => 'single_text',
                      'attr' => array('class' => 'form-control'),
                    ))
                    
            ->add('nombre', 'genemu_jqueryselect2_choice', array(
                            'choices' => array(
                                            'Sandra' => 'Sandra',
                                            'David' => 'David',
                                            'Hugo' => 'Hugo'),
                'attr' => array('class' => 'form-control'),
                'empty_value'=>'Seleccione...'))
                    
            ->add('monto','integer',array( 'attr' => array('class' => 'form-control','placeholder' => '0')))
            ->add('obs','text',array( 'attr' => array('class' => 'form-control','required' => false,'empty_value'=>'null')))
            
            ->add('cajaconcepto_id', 'genemu_jqueryselect2_entity', array(
                    'class' => 'GestionCajaBundle:CajaConcepto',
                    'property' => 'concepto',
                    'attr' => array('class' => 'form-control'),
                    'required' => 'true',
                    'empty_value'=>'Seleccione...',
                    'query_builder' => function (\Doctrine\ORM\EntityRepository $repository)
                 {
                     // Muestra solo conceptos de tipo egreso
                     return $repository->createQueryBuilder('c')
                            ->where('c.tipo = ?1')
                            ->andwhere('c.nulo = ?2')
                            ->orderby('c.concepto')
                            ->setParameter(1, '2')
                            ->setParameter(2, '0');
                 }
            ))
                    
            ->add('moneda_id', 'genemu_jqueryselect2_entity', array(
                    'class' => 'GestionCajaBundle:Moneda',
                    'property' => 'moneda',
                'attr' => array('class' => 'form-control'),
                'empty_value'=>'Seleccione...'
        ))

            ->add('Ingresar','submit',array( 'attr' => array('class' => 'btn btn-danger')))
                    
            ->getform();

            $form->handleRequest($request);

            if ($form->isValid()) {

                $fecha_caja= $form["fecha"]->getData();
                
                $fecha_caja->format('Y-m-d');
                
                
                $em = $this->getDoctrine()->getManager();
                $cierre = $em->getRepository('GestionCajaBundle:CajaCierre')
                         ->ultimoCierre();
                $cierre= array_shift($cierre['0']);

                $fecha_cierre = \DateTime::createFromFormat('Y-m-d', $cierre);
                
                if ( $fecha_caja <= $fecha_cierre ) {
                    
                    $msg='Ud. esta intentando ingresar un egreso con fecha ' . $fecha_caja->format('d/m/Y') . 
                         ' y ese dia ya esta cerrado.';
                    return $this->render
                    ('GestionCajaBundle:Default:error.html.twig', 
                    array('msg'=>$msg)
                    );
                }
                $hoy = date('Y-m-d');
               
                $fecha_hoy = \DateTime::createFromFormat('Y-m-d', $hoy);
                
                
                if ( $fecha_caja > $fecha_hoy ) {
                    $msg='Ud. esta intentando ingresar un ingreso con fecha ' . $fecha_caja->format('d/m/Y') . 
                         ' no puede ingresar movimientos con fecha futura.';
                    return $this->render
                    ('GestionCajaBundle:Default:error.html.twig', 
                    array('msg'=>$msg)
                    );
                }

                $caja->setFecha($fecha_caja);
                $caja->setHora(new \DateTime("now"));
                $caja->setTipo('2');
                $caja->setUsuario($this->getUser());
                $caja->setNula(0);
                //Capitaliza nombre
                //$pax=$form["pax"]->getData(); 
                //$cpax = ucwords(strtolower($pax));
                //$caja->setPax($cpax);
                $em= $this->getDoctrine()->getManager();
                $em->persist($caja);
                $em->flush();
                return $this->redirect($this->generateUrl('gestion_caja'));
            }
            
            //Datos NAVBAR
            $em = $this->getDoctrine()->getEntityManager();
            $saldo_actual_clp = $em->getRepository('GestionCajaBundle:CajaCierre')
                         ->getSaldoActual(1);
            $saldo_actual_usd = $em->getRepository('GestionCajaBundle:CajaCierre')
                         ->getSaldoActual(2);  
            return $this->render
                    ('GestionCajaBundle:Default:egreso.html.twig', 
                    array("form"=>$form->createview(),
                        'saldo_actual_clp'=>$saldo_actual_clp,
                        'saldo_actual_usd'=>$saldo_actual_usd,
                        //"valor_usd"=>$valor_dolar,
                        //"valor_euro"=>$valor_euro
                        )
                    );
    }    
    
    public function addingresoAction(Request $request)
            
    {
        
        $caja = new Caja();

        $caja->setFecha(new \DateTime("now"));

        $form = $this->createFormBuilder($caja)
        ->add('fecha', 'genemu_jquerydate',array('widget'  => 'single_text',
                  'attr' => array('class' => 'form-control',
                  )                                               ))
        ->add('nombre', 'genemu_jqueryselect2_choice', array(
                        'choices' => array(
                                        'Sandra' => 'Sandra',
                                        'David' => 'David',
                                        'Hugo' => 'Hugo'),
            'attr' => array('class' => 'form-control'),
            'empty_value'=>'Seleccione...'))

        ->add('monto','integer',array( 'attr' => array('class' => 'form-control','placeholder' => '0')))
        ->add('obs','text',array( 'attr' => array('class' => 'form-control','required' => false,'empty_value'=>'null')))
        ->add('hab', 'genemu_jqueryselect2_choice', array(

                        'choices' => array(
                                        '0' => '0',
                                        '1' => '1',
                                        '2' => '2',
                                        '3' => '3',
                                        '4' => '4',
                                        '5' => '5',
                                        '6' => '6',
                                        '7' => '7',
                                        '8' => '8',
                                        '9' => '9'),
                        'empty_value'=>'Seleccione...',
                        'attr' => array('class' => 'form-control')))
        ->add('pax', 'text',array('attr' => array('class' => 'form-control', )  ))
        ->add('doc', 'integer',array('attr' => array('class' => 'form-control', 'data' => '0' )  ))
        ->add('cajaconcepto_id', 'genemu_jqueryselect2_entity', array(
                'class' => 'GestionCajaBundle:CajaConcepto',
                'property' => 'concepto',
            'attr' => array('class' => 'form-control'),
            'required' => 'true',
            'query_builder' => function (\Doctrine\ORM\EntityRepository $repository)
             {
                 // Muestra solo conceptos de tipo egreso
                 return $repository->createQueryBuilder('c')
                        ->where('c.tipo = ?1')
                         ->orderby('c.concepto')
                        ->setParameter(1, '1');
             }
        ))

        ->add('moneda_id', 'genemu_jqueryselect2_entity', array(
                'class' => 'GestionCajaBundle:Moneda',
                'property' => 'moneda',
            'attr' => array('class' => 'form-control' ),
            'empty_value' => 'Seleccione...'
    ))

            ->add('Ingresar','submit',array( 'attr' => array('class' => 'btn btn-info')))
                    
            ->getform();

            $form->handleRequest($request);

            if ($form->isValid()) {
                    
                // Valida fecha caja > fecha ultimo cierre
                //$fecha_caja=$caja->getFecha();
                //var_dump($cierre_fecha);
          
                
                $fecha_caja= $form["fecha"]->getData();
                //$fecha_caja->modify('+1 day');
                $fecha_caja->format('Y-m-d');
                
                
                $em = $this->getDoctrine()->getManager();
                $cierre = $em->getRepository('GestionCajaBundle:CajaCierre')
                         ->ultimoCierre();
                $cierre= array_shift($cierre['0']);

                $fecha_cierre = \DateTime::createFromFormat('Y-m-d', $cierre);
                
                // Valida formulario
                if ( $fecha_caja <= $fecha_cierre ) {
                    $msg='Ud. esta intentando ingresar un ingreso con fecha ' . $fecha_caja->format('d/m/Y') . 
                         ' y ese dia ya esta cerrado.';
                    return $this->render
                    ('GestionCajaBundle:Default:error.html.twig', 
                    array('msg'=>$msg)
                    );
                }
                $hoy = date('Y-m-d');
                //var_dump($hoy);
                $fecha_hoy = \DateTime::createFromFormat('Y-m-d', $hoy);
                //var_dump($fecha_hoy);
                
                if ( $fecha_caja > $fecha_hoy ) {
                    $msg='Ud. esta intentando ingresar un ingreso con fecha ' . $fecha_caja->format('d/m/Y') . 
                         ' no puede ingresar movimientos con fecha futura.';
                    return $this->render
                    ('GestionCajaBundle:Default:error.html.twig', 
                    array('msg'=>$msg)
                    );
                }
                
                
                $caja->setFecha($fecha_caja);
                $caja->setTipo('1');
                $hora= date('HH:mm:ss');
                $caja->setHora(new \DateTime("now"));
                $caja->setUsuario($this->getUser());
                $caja->setNula(0);
                //Capitaliza nombre
                $pax=$form["pax"]->getData(); 
                $cpax = ucwords(strtolower($pax));
                $caja->setPax($cpax);
                $em= $this->getDoctrine()->getManager();
                $em->persist($caja);
                $em->flush();

                return $this->redirect($this->generateUrl('gestion_caja'));
            }
            //Datos NAVBAR
            $em = $this->getDoctrine()->getEntityManager();
            $saldo_actual_clp = $em->getRepository('GestionCajaBundle:CajaCierre')
                         ->getSaldoActual(1);
            $saldo_actual_usd = $em->getRepository('GestionCajaBundle:CajaCierre')
                         ->getSaldoActual(2);  

            return $this->render
                    ('GestionCajaBundle:Default:ingreso.html.twig', 
                    array("form"=>$form->createview(),
                        'saldo_actual_clp'=>$saldo_actual_clp,
                        'saldo_actual_usd'=>$saldo_actual_usd,
                        //"valor_usd"=>$valor_dolar,
                        //"valor_euro"=>$valor_euro
                        )
                    );
    }    

    public function anularcajaAction($id)
	{
        $em = $this->getDoctrine()->getEntityManager();
            $caja = $em->getRepository('GestionCajaBundle:Caja')->find($id);
            $caja->setNula(1);
            $em->persist($caja);
            $flush = $em->flush();
            if ($flush == null) {
                    echo "Caja anulada correctamente";
            } else {
                    echo "La caja no se ha anulado";
            }
            return $this->redirect($this->generateUrl('gestion_caja'));
	}

    public function habilitarcajaAction($id)
	{
            $em = $this->getDoctrine()->getEntityManager();
            $caja = $em->getRepository('GestionCajaBundle:Caja')->find($id);
            $caja->setNula(0);
            $em->persist($caja);
            $flush = $em->flush();
            if ($flush == null) {
                    echo "Caja habilitada correctamente";
            } else {
                    echo "La caja no se ha habilitar";
            }
            return $this->redirect($this->generateUrl('gestion_caja'));
	}
    
    public function cerrarcajaAction($p_saldo_actual, $d_saldo_actual)
	{
        
            // Valida que el dia no este ya cerrado
            $em = $this->getDoctrine()->getManager();
            $cierre = $em->getRepository('GestionCajaBundle:CajaCierre')
                       ->ultimoCierre();
            $cierre= array_shift($cierre['0']);
            
            $cierre2 = \DateTime::createFromFormat('Y-m-d', $cierre);
            //var_dump($cierre2);
            
            $ob1 = new Highchart();
            $ob2 = new Highchart();
            
            $hoy=new \DateTime("now");
            if ($cierre2  >= $hoy) {
            //    var_dump($cierre2);
                $msg='Ud. esta intentando cerrar el dia ' . $hoy->format('d/m/Y') . 
                     ' y ese dia ya esta cerrado.';
                return $this->render
                ('GestionCajaBundle:Default:error.html.twig', 
                array('msg'=>$msg,
                      'chart_1' => $ob1,
                      'chart_2' => $ob2)
                );
            }

            // Pesos
            $CajaCierre = new CajaCierre();
            $CajaCierre->setCierre(new \DateTime("now"));
            $CajaCierre->setFecha(new \DateTime("now"));
            $CajaCierre->setHora(new \DateTime("now"));
            $CajaCierre->setSaldo($p_saldo_actual);
            $CajaCierre->setMonedaId('1');
            $CajaCierre->setUsuario($this->getUser());
             $em=$this->getDoctrine()->getManager();
            $em->persist($CajaCierre);
            $flush = $em->flush();
            
            // Dolar
            $CajaCierre = new CajaCierre();
            $CajaCierre->setCierre(new \DateTime("now"));
            $CajaCierre->setFecha(new \DateTime("now"));
            $CajaCierre->setHora(new \DateTime("now"));
            $CajaCierre->setSaldo($d_saldo_actual);
            $CajaCierre->setMonedaId('2');
            $CajaCierre->setUsuario($this->getUser());
             $em=$this->getDoctrine()->getManager();
            $em->persist($CajaCierre);
            $flush = $em->flush();
            
            if ($flush == null) {
                    echo "Caja cerrada correctamente";
            } else {
                    echo "La caja no se ha cerrado";
            }
            return $this->redirect($this->generateUrl('gestion_caja'));

	}
}

