<?php

namespace Gestion\TraspasosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * traspasos_tours
 *
 * @ORM\Table(name="traspasos_tours", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 */

class TraspasosTours
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="tour", type="string", nullable=false)
     */
    private $tour;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tour
     *
     * @param string $tour
     *
     * @return TraspasosTours
     */
    public function setTour($tour)
    {
        $this->tour = $tour;

        return $this;
    }

    /**
     * Get tour
     *
     * @return string
     */
    public function getTour()
    {
        return $this->tour;
    }

 
    
    
    
    /**
    * @ORM\ManyToOne(targetEntity="Traspasos", inversedBy="traspaso")  
    * @ORM\JoinColumn(name="traspasotour_id", referencedColumnName="id")
    **/
    
    protected $traspasostours;
    
    
 public function __toString()
    {
        return strval($this->id);
    }
    
    
}

