<?php
namespace Gestion\TraspasosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Condition\ConditionBuilderInterface;

class filtroInfFechaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder->add('fecha_traspaso','filter_date_range', array(
                'left_date_options' => array('label' => 'Desde:',
                							  'widget' => 'single_text',
                							  'data' => new \DateTime(date('Y-m-01'))),
                'right_date_options' => array('label' => 'Hasta:',
                		                       'widget' => 'single_text',
                                                        'data' => new \DateTime("now"))));	
    }

    public function getName()
    {
        return 'filtro_traspasos';
    }
}