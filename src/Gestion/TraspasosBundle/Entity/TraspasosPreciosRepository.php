<?php 

namespace Gestion\TraspasosBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TraspasosPreciosRepository extends EntityRepository
{

    public function precio($to_id, $tour_id)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p.precio
                FROM GestionTraspasosBundle:TraspasosPrecios p 
                WHERE p.to_id = :to_id AND p.tour_id = :tour_id')
            ->setParameter('to_id', $to_id)
            ->setParameter('tour_id', $tour_id)    
            ->getResult();
    }   
    
    }