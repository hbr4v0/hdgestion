<?php

namespace Gestion\CajaBundle\Controller;

use Gestion\CajaBundle\Entity\Caja;
use Gestion\CajaBundle\Entity\CajaRepository;
use Gestion\CajaBundle\Entity\CajaCierre;
use Gestion\CajaBundle\Entity\CajaCierreRepository;
use Gestion\CajaBundle\Entity\CajaConcepto;
use Gestion\CajaBundle\Entity\Moneda;
use Gestion\TraspasosBundle\Entity\TraspasosRepository;
use Gestion\TraspasosBundle\Entity\Traspasos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Gestion\CajaBundle\Form\filtroBuscarCajasType;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Gestion\CajaBundle\Controller\MiscController;

class InformesController extends Controller
{
    
    public function indexAction()
    {
            //return $this->render('GestionMainBundle:Default:index.html.twig', array('name' => $name));
    }

        
    public function obtieneDetalleMov($fecha, $cat_id,$cat ,$moneda_id, $tipo)
    {
        // ++++++++++++++++++++++++++++++++ //
        // CREA STRING CON DETALLE DE MOVS  //
        // ++++++++++++++++++++++++++++++++ //
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
        "SELECT c.nula, c.id, c.fecha, c.hora, c.nombre, o.concepto, c.monto, c.obs
        FROM GestionCajaBundle:Caja c 
        inner JOIN GestionCajaBundle:CajaConcepto o
        with c.cajaconcepto_id = o.id
        WHERE (c.fecha='" . $fecha . "') AND (c.tipo=" . $tipo  . ") AND (c.moneda_id=" . $moneda_id .  ") AND (o.cat_id=" . $cat_id . ")
        GROUP BY c.nula, c.id, c.fecha, c.hora, c.nombre, o.concepto, c.monto, c.obs"
        ); 

        $ret = $query->getArrayResult();
        
        // Devuelve string

        $fecha = \DateTime::createFromFormat('Y-m-d', $fecha);

        $fecha = $fecha->format('d/M/Y');;
        $s="";
        $s=$s . "<div class='panel panel-default panel-primary'>";
        $s=$s . "<div class='panel-heading text-left'>" . strtoupper($cat) . " - " . $fecha . "</div>"; 
        $s=$s . "<table class='table table-bordered table-hover table-condensed table-responsive'>";
        $s=$s . "<tr>";
        $s=$s . "<th>";
        $s=$s . " ID"; 
        $s=$s . "</th>";       
        $s=$s . "<th>";
        $s=$s . " HORA";
        $s=$s . "</th>";
        $s=$s . "<th>";
        $s=$s . " NOMBRE";
        $s=$s . "</th>";
        $s=$s . "<th>";
        $s=$s . "CONCEPTO";
        $s=$s . "</th>";
        $s=$s . "<th>";
        $s=$s . "MONTO";
        $s=$s . "</th>";
        $s=$s . "<th>";
        $s=$s . "OBS";
        $s=$s . "</th>";            
        $s=$s . "</tr>";
        $total=0;
        foreach($ret as $r) 
        {
            if ($r['nula']=='1'){
                $s=$s . "<tr class='danger'>";
            }
            else
            {
                $s=$s . "<tr>";
                $total=$total + $r['monto'];
            }
            
            $s=$s . "<td>";
            $s=$s . $r['id'];
            $s=$s . "</td>";
            $s=$s . "<td>";
            $s=$s . $r['hora']->format("h:i");
            $s=$s . "</td>";            
            $s=$s . "<td>" . $r['nombre'] . "</td>";
            $s=$s . "<td>" . $r['concepto'] . "</td>";
            $s=$s . "<td class='text-right'>" . number_format($r['monto']) . "</td>";
            $s=$s . "<td>" . $r['obs'] . "</td>";
            $s=$s . "</tr>";
        }   

        // FILA TOTAL
        $s=$s . "<tr>";
        $s=$s . "<td></td>";
        $s=$s . "<td></td>";
        $s=$s . "<td></td>";
        $s=$s . "<th>TOTAL</th>";
        $s=$s . "<th class='text-right'>$ " . number_format($total) . "</th>";
        $s=$s . "<td></td>";
        $s=$s . "</tr>";
        $s=$s . "</table>";
        $s= $s . "</div>";
        $s= $s . "</div>";
        
        return $s;
    }
        
    public function dashAction()
    {
        
        $em = $this->getDoctrine()->getManager();
        
        //Obtiene Saldos y CCs
        
        $saldo_actual_clp = $em->getRepository('GestionCajaBundle:CajaCierre')
                     ->getSaldoActual(1);
        $saldo_actual_usd = $em->getRepository('GestionCajaBundle:CajaCierre')
                     ->getSaldoActual(2);  
        $nmes = date('m');
        //var_dump($nmes);
        $tours_por_cobrar = $em->getRepository('GestionTraspasosBundle:Traspasos')
                     ->getToursPorCobrar($nmes);  

        // +++++++++++++++++++++++}}}}}}}}}}}}} //
        // RESUMEN EGRESOS DIAS OPERACIONAL  //
        // +++++++++++++++++++++++ //
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
        'SELECT c.id, c.categoria    
         FROM GestionCajaBundle:CajaConceptoCateg c 
         WHERE c.tipo = :tipo AND c.operacional = 1  
         ORDER BY c.categoria ASC'
        )->setParameter('tipo', '2'); // Egresos

        $arr_cats_op = $query->getArrayResult();
        
        $fecha = new \DateTime("now");
        
        for ($i = 0; $i <= 6; $i++)
        {
            if ($i>0){
                $fecha->modify('-1 day');
            }
            $dia = $fecha->format("Y-m-d");
            $arr_res_dia_op_dias[$i] = array('dia'=>$dia);
        } 
        
        $i=0; $tot00=0; $tot01=0; $tot02=0; $tot03=0; $tot04=0; $tot05=0; $tot06=0;
        
        foreach($arr_cats_op as $categ) 
        {
            
            $id_cat=$categ['id'];
            $cat=$categ['categoria'];
            
            $dia = $arr_res_dia_op_dias[0]['dia'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
             WHERE (((c.fecha)='" . $dia . "') AND ((g.id)='" . $id_cat . "')) AND (c.nula=0) AND (c.moneda_id=1)"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor00 = $valor;
            $detalle00[$i]= $this->obtieneDetalleMov($dia, $id_cat,$cat,1,2);
            
            // **** 01
            $dia = $arr_res_dia_op_dias[1]['dia'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (((c.fecha)='" . $dia . "') AND ((g.id)='" . $id_cat . "')) AND (c.nula=0) AND (c.moneda_id=1)"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor01 = $valor;
            $detalle01[$i]= $this->obtieneDetalleMov($dia,$id_cat,$cat,1,2);
            // **** 02
            $dia = $arr_res_dia_op_dias[2]['dia'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (((c.fecha)='" . $dia . "') AND ((g.id)='" . $id_cat . "')) AND (c.nula=0) AND (c.moneda_id=1)"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor02 = $valor;
            $detalle02[$i]= $this->obtieneDetalleMov($dia,$id_cat,$cat,1,2);
            // **** 03
            $dia = $arr_res_dia_op_dias[3]['dia'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (((c.fecha)='" . $dia . "') AND ((g.id)='" . $id_cat . "')) AND (c.nula=0) AND (c.moneda_id=1)"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor03 = $valor;
            $detalle03[$i]= $this->obtieneDetalleMov($dia,$id_cat,$cat,1,2);
            // **** 04
            $dia = $arr_res_dia_op_dias[4]['dia'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (((c.fecha)='" . $dia . "') AND ((g.id)='" . $id_cat . "')) AND (c.nula=0) AND (c.moneda_id=1)"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor04 = $valor;
            $detalle04[$i]= $this->obtieneDetalleMov($dia,$id_cat,$cat,1,2);
            // **** 05
            $dia = $arr_res_dia_op_dias[5]['dia'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (((c.fecha)='" . $dia . "') AND ((g.id)='" . $id_cat . "')) AND (c.nula=0) AND (c.moneda_id=1)"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor05 = $valor;
            $detalle05[$i]= $this->obtieneDetalleMov($dia,$id_cat,$cat,1,2);
            // **** 06
            $dia = $arr_res_dia_op_dias[6]['dia'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (((c.fecha)='" . $dia . "') AND ((g.id)='" . $id_cat . "')) AND (c.nula=0) AND (c.moneda_id=1)"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor06 = $valor;
            $detalle06[$i]= $this->obtieneDetalleMov($dia,$id_cat,$cat,1,2);
            
            $arr_res_dia_op[$i] = array(
                'dia'=>$dia,
                'id'=>$id_cat,
                'categoria'=>$cat,
                'valor00'=>$valor00,
                'valor01'=>$valor01,
                'valor02'=>$valor02,
                'valor03'=>$valor03,
                'valor04'=>$valor04,
                'valor05'=>$valor05,
                'valor06'=>$valor06,
                'detalle00'=>$detalle00,
                'detalle01'=>$detalle01,
                'detalle02'=>$detalle02,
                'detalle03'=>$detalle03,
                'detalle04'=>$detalle04,
                'detalle05'=>$detalle05,
                'detalle06'=>$detalle06,
                );
            $i=$i+1;
            
            //Guarda totales en array para graficarlos
            $tot00=$tot00 + $valor00;
            $tot01=$tot01 + $valor01;
            $tot02=$tot02 + $valor02;
            $tot03=$tot03 + $valor03;
            $tot04=$tot04 + $valor04;
            $tot05=$tot05 + $valor05;
            $tot06=$tot06 + $valor06;
        }
        $arr_gra_dia_op_data= array($tot00,$tot01,$tot02,$tot03,$tot04,$tot05,$tot06);
        $arr_gra_dia_op_cat = array(
            date("d/M", strtotime($arr_res_dia_op_dias[0]['dia'])),
            date("d/M", strtotime($arr_res_dia_op_dias[1]['dia'])),
            date("d/M", strtotime($arr_res_dia_op_dias[2]['dia'])),
            date("d/M", strtotime($arr_res_dia_op_dias[3]['dia'])),
            date("d/M", strtotime($arr_res_dia_op_dias[4]['dia'])),
            date("d/M", strtotime($arr_res_dia_op_dias[5]['dia'])),
            date("d/M", strtotime($arr_res_dia_op_dias[6]['dia'])),
            ) ;
        
        // +++++++++++++++++++++++}}}}}}}}}}}}} //
        // RESUMEN EGRESOS DIAS NO OPERACIONAL  //
        // +++++++++++++++++++++++ //
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
        'SELECT c.id, c.categoria    
         FROM GestionCajaBundle:CajaConceptoCateg c 
         WHERE c.tipo = :tipo AND c.operacional = 2  
         ORDER BY c.categoria ASC'
        )->setParameter('tipo', '2'); // Egresos

        $arr_cats_nop = $query->getArrayResult();
        
        $fecha = new \DateTime("now");
        
        for ($i = 0; $i <= 6; $i++)
        {
            if ($i>0){
            $fecha->modify('-1 day');
            }
            $dia= $fecha->format("Y-m-d");
            $arr_res_dia_nop_dias[$i] = array('dia'=>$dia);
        } 
        
        $i=0; $tot00=0; $tot01=0; $tot02=0; $tot03=0; $tot04=0; $tot05=0; $tot06=0;
        
        foreach($arr_cats_nop as $categ) 
        {
            
            $id_cat=$categ['id'];
            $cat=$categ['categoria'];
            
            $dia = $arr_res_dia_nop_dias[0]['dia'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
             WHERE (((c.fecha)='" . $dia . "') AND ((g.id)='" . $id_cat . "')) AND (c.nula=0) AND (c.moneda_id=1)"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor00 = $valor;
            $detalle00[$i]= $this->obtieneDetalleMov($dia, $id_cat,$cat,1,2);
            
            // **** 01
            $dia = $arr_res_dia_nop_dias[1]['dia'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (((c.fecha)='" . $dia . "') AND ((g.id)='" . $id_cat . "')) AND (c.nula=0) AND (c.moneda_id=1)"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor01 = $valor;
            $detalle01[$i]= $this->obtieneDetalleMov($dia,$id_cat,$cat,1,2);
            // **** 02
            $dia = $arr_res_dia_nop_dias[2]['dia'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (((c.fecha)='" . $dia . "') AND ((g.id)='" . $id_cat . "')) AND (c.nula=0) AND (c.moneda_id=1)"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor02 = $valor;
            $detalle02[$i]= $this->obtieneDetalleMov($dia,$id_cat,$cat,1,2);
            // **** 03
            $dia = $arr_res_dia_nop_dias[3]['dia'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (((c.fecha)='" . $dia . "') AND ((g.id)='" . $id_cat . "')) AND (c.nula=0) AND (c.moneda_id=1)"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor03 = $valor;
            $detalle03[$i]= $this->obtieneDetalleMov($dia,$id_cat,$cat,1,2);
            // **** 04
            $dia = $arr_res_dia_nop_dias[4]['dia'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (((c.fecha)='" . $dia . "') AND ((g.id)='" . $id_cat . "')) AND (c.nula=0) AND (c.moneda_id=1)"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor04 = $valor;
            $detalle04[$i]= $this->obtieneDetalleMov($dia,$id_cat,$cat,1,2);
            // **** 05
            $dia = $arr_res_dia_nop_dias[5]['dia'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (((c.fecha)='" . $dia . "') AND ((g.id)='" . $id_cat . "')) AND (c.nula=0) AND (c.moneda_id=1)"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor05 = $valor;
            $detalle05[$i]= $this->obtieneDetalleMov($dia,$id_cat,$cat,1,2);
            // **** 06
            $dia = $arr_res_dia_nop_dias[6]['dia'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (((c.fecha)='" . $dia . "') AND ((g.id)='" . $id_cat . "')) AND (c.nula=0) AND (c.moneda_id=1)"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor06 = $valor;
            $detalle06[$i]= $this->obtieneDetalleMov($dia,$id_cat,$cat,1,2);
            
            $arr_res_dia_nop[$i] = array(
                'dia'=>$dia,
                'id'=>$id_cat,
                'categoria'=>$cat,
                'valor00'=>$valor00,
                'valor01'=>$valor01,
                'valor02'=>$valor02,
                'valor03'=>$valor03,
                'valor04'=>$valor04,
                'valor05'=>$valor05,
                'valor06'=>$valor06,
                'detalle00'=>$detalle00,
                'detalle01'=>$detalle01,
                'detalle02'=>$detalle02,
                'detalle03'=>$detalle03,
                'detalle04'=>$detalle04,
                'detalle05'=>$detalle05,
                'detalle06'=>$detalle06,
                );
            $i=$i+1;

        }

        // +++++++++++++++++++++++ //
        // RESUMEN EGRESOS OPERACIONALES 6 MESES //
        // +++++++++++++++++++++++ //

        $hoy = new \DateTime("now");
        
        for ($i = 0; $i <= 5; $i++)
        {
            if ($i>0){
                $hoy->modify('-1 month');
            }
            $mes=$hoy->format("M") . "/" . $hoy->format("y");
            $nmes=$hoy->format("m");
            
            $mes_desde = $hoy->format('Y') . "-" . $hoy->format('m') . "-01";

            $anio=date("Y");
            $dias_mes= date("d",(mktime(0,0,0,$nmes+1,1,$anio)-1));
            $dias=intval($dias_mes);

            $mes_hasta = $hoy->format('Y') . "-" . $hoy->format('m') . "-" . $dias;

            $arr_res_mes_op_fechas[$i] = array('mes'=>$mes,'mes_desde'=>$mes_desde,'mes_hasta'=>$mes_hasta);
        } 

        $i=0;  
        
        $tot00=0;
        $tot01=0;
        $tot02=0;
        $tot03=0;
        $tot04=0;
        $tot05=0;
        
        foreach($arr_cats_op as $categ) 
        {
            
            $id_cat=$categ['id'];
            $cat=$categ['categoria'];
            $mes_desde = $arr_res_mes_op_fechas[0]['mes_desde'];
            $mes_hasta = $arr_res_mes_op_fechas[0]['mes_hasta'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
             WHERE (c.fecha BETWEEN '" . $mes_desde . "' AND '" . $mes_hasta . "') AND (g.id='" . $id_cat . "')"
            )->setMaxResults(10);

            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor00 = $valor;
            // **** 01
            $mes_desde = $arr_res_mes_op_fechas[1]['mes_desde'];
            $mes_hasta = $arr_res_mes_op_fechas[1]['mes_hasta'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (c.fecha BETWEEN '" . $mes_desde . "' AND '" . $mes_hasta . "') AND (g.id='" . $id_cat . "')"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor01 = $valor;
            // **** 02
            $mes_desde = $arr_res_mes_op_fechas[2]['mes_desde'];
            $mes_hasta = $arr_res_mes_op_fechas[2]['mes_hasta'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (c.fecha BETWEEN '" . $mes_desde . "' AND '" . $mes_hasta . "') AND (g.id='" . $id_cat . "')"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor02 = $valor;
            // **** 03
            $mes_desde = $arr_res_mes_op_fechas[3]['mes_desde'];
            $mes_hasta = $arr_res_mes_op_fechas[3]['mes_hasta'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (c.fecha BETWEEN '" . $mes_desde . "' AND '" . $mes_hasta . "') AND (g.id='" . $id_cat . "')"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor03 = $valor;
            // **** 04
            $mes_desde = $arr_res_mes_op_fechas[4]['mes_desde'];
            $mes_hasta = $arr_res_mes_op_fechas[4]['mes_hasta'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (c.fecha BETWEEN '" . $mes_desde . "' AND '" . $mes_hasta . "') AND (g.id='" . $id_cat . "')"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor04 = $valor;
            // **** 05
            $mes_desde = $arr_res_mes_op_fechas[5]['mes_desde'];
            $mes_hasta = $arr_res_mes_op_fechas[5]['mes_hasta'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (c.fecha BETWEEN '" . $mes_desde . "' AND '" . $mes_hasta . "') AND (g.id='" . $id_cat . "')"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor05 = $valor;
            
            $arr_res_mes_op[$i] = array(
                'mes'=>$mes,
                'id'=>$id_cat,
                'categoria'=>$cat,
                'valor00'=>$valor00,
                'valor01'=>$valor01,
                'valor02'=>$valor02,
                'valor03'=>$valor03,
                'valor04'=>$valor04,
                'valor05'=>$valor05,
                );

            $i=$i+1;
            
            //Guarda totales en array para graficarlos
            $tot00=$tot00 + $valor00;
            $tot01=$tot01 + $valor01;
            $tot02=$tot02 + $valor02;
            $tot03=$tot03 + $valor03;
            $tot04=$tot04 + $valor04;
            $tot05=$tot05 + $valor05;
        } 
        
        $arr_gra_mes_op_data= array($tot00,$tot01,$tot02,$tot03,$tot04,$tot05);
        $arr_gra_mes_op_cat = array(
        $arr_res_mes_op_fechas[0]['mes'],
        $arr_res_mes_op_fechas[1]['mes'],
        $arr_res_mes_op_fechas[2]['mes'],
        $arr_res_mes_op_fechas[3]['mes'],
        $arr_res_mes_op_fechas[4]['mes']   ,
        $arr_res_mes_op_fechas[5]['mes'],
        ) ;
        
        // +++++++++++++++++++++++ //
        // RESUMEN EGRESOS NOOPERACIONALES 6 MESES //
        // +++++++++++++++++++++++ //
        
        $hoy = new \DateTime("now");
        
        for ($i = 0; $i <= 5; $i++)
        {
            if ($i>0){
                $hoy->modify('-1 month');
            }
            $mes=$hoy->format("M") . "/" . $hoy->format("y");
            $nmes=$hoy->format("m");
            
            $mes_desde = $hoy->format('Y') . "-" . $hoy->format('m') . "-01";

            $anio=date("Y");
            $dias_mes= date("d",(mktime(0,0,0,$nmes+1,1,$anio)-1));
            $dias=intval($dias_mes);

            $mes_hasta = $hoy->format('Y') . "-" . $hoy->format('m') . "-" . $dias;

            $arr_res_mes_nop_fechas[$i] = array('mes'=>$mes,'mes_desde'=>$mes_desde,'mes_hasta'=>$mes_hasta);
            
        } 

        $i=0;  

        
        foreach($arr_cats_nop as $categ) 
        {
            
            $id_cat=$categ['id'];
            $cat=$categ['categoria'];
            $mes_desde = $arr_res_mes_nop_fechas[0]['mes_desde'];
            $mes_hasta = $arr_res_mes_nop_fechas[0]['mes_hasta'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
             WHERE (c.fecha BETWEEN '" . $mes_desde . "' AND '" . $mes_hasta . "') AND (g.id='" . $id_cat . "')"
            )->setMaxResults(10);

            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor00 = $valor;
            // **** 01
            $mes_desde = $arr_res_mes_op_fechas[1]['mes_desde'];
            $mes_hasta = $arr_res_mes_op_fechas[1]['mes_hasta'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (c.fecha BETWEEN '" . $mes_desde . "' AND '" . $mes_hasta . "') AND (g.id='" . $id_cat . "')"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor01 = $valor;
            // **** 02
            $mes_desde = $arr_res_mes_op_fechas[2]['mes_desde'];
            $mes_hasta = $arr_res_mes_op_fechas[2]['mes_hasta'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (c.fecha BETWEEN '" . $mes_desde . "' AND '" . $mes_hasta . "') AND (g.id='" . $id_cat . "')"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor02 = $valor;
            // **** 03
            $mes_desde = $arr_res_mes_op_fechas[3]['mes_desde'];
            $mes_hasta = $arr_res_mes_op_fechas[3]['mes_hasta'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (c.fecha BETWEEN '" . $mes_desde . "' AND '" . $mes_hasta . "') AND (g.id='" . $id_cat . "')"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor03 = $valor;
            // **** 04
            $mes_desde = $arr_res_mes_op_fechas[4]['mes_desde'];
            $mes_hasta = $arr_res_mes_op_fechas[4]['mes_hasta'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (c.fecha BETWEEN '" . $mes_desde . "' AND '" . $mes_hasta . "') AND (g.id='" . $id_cat . "')"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor04 = $valor;
            // **** 05
            $mes_desde = $arr_res_mes_op_fechas[5]['mes_desde'];
            $mes_hasta = $arr_res_mes_op_fechas[5]['mes_hasta'];
            $query = $em->createQuery(
            "SELECT Sum(c.monto) AS monto
             FROM GestionCajaBundle:Caja c 
             inner JOIN GestionCajaBundle:CajaConcepto o
             with c.cajaconcepto_id = o.id
             inner JOIN GestionCajaBundle:CajaConceptoCateg g
             with o.cat_id = g.id
            WHERE (c.fecha BETWEEN '" . $mes_desde . "' AND '" . $mes_hasta . "') AND (g.id='" . $id_cat . "')"
            )->setMaxResults(10);
            $result = $query->getArrayResult();
            $valor = array_shift($result['0']);
            $valor05 = $valor;
            
            $arr_res_mes_nop[$i] = array(
                'mes'=>$mes,
                'id'=>$id_cat,
                'categoria'=>$cat,
                'valor00'=>$valor00,
                'valor01'=>$valor01,
                'valor02'=>$valor02,
                'valor03'=>$valor03,
                'valor04'=>$valor04,
                'valor05'=>$valor05,
                );

            $i=$i+1;
            
            //Guarda totales en array para graficarlos
            //$tot00=$tot00 + $valor00;
            //$tot01=$tot01 + $valor01;
            //$tot02=$tot02 + $valor02;
            //$tot03=$tot03 + $valor03;
            //$tot04=$tot04 + $valor04;
            //$tot05=$tot05 + $valor05;
        } 
        
        //$arr_gra_mes_op_data= array($tot00,$tot01,$tot02,$tot03,$tot04,$tot05);
        //$arr_gra_mes_op_cat = array(
        //$arr_res_mes_op_fechas[0]['mes'],
        //$arr_res_mes_op_fechas[1]['mes'],
        //$arr_res_mes_op_fechas[2]['mes'],
        //$arr_res_mes_op_fechas[3]['mes'],
        //$arr_res_mes_op_fechas[4]['mes']   ,
        //$arr_res_mes_op_fechas[5]['mes'],
        //) ;
        
        // GRAFICO EGRESOS DIAS OPERACIONALES
        $series = array(
        array("name" => "Total egresos dia","data" => $arr_gra_dia_op_data) 
        );
        $ob1 = new Highchart();
        $ob1->chart->renderTo('chart_1');  // The #id of the div where to render the chart
        $ob1->chart->type('line');
        $ob1->title->text('Egresos Operacionales');
        $ob1->subtitle->text('Ultimos 7 dias');
        $ob1->yAxis->title(array('text'  => "Miles CLP"));
        $ob1->xAxis->categories($arr_gra_dia_op_cat);
        $ob1->plotOptions->line(array(
        'allowPointSelect'  => true,
        'dataLabels'    => array('enabled' => true),
        'numericsymbols'  => false
        ));
        $ob1->series($series);
        
        // GRAFICO EGRESOS OPERACIONALES 6 MESES
        $series = array(
        array("name" => "Total egresos mes","data" => $arr_gra_mes_op_data) 
        );
        $ob2 = new Highchart();
        $ob2->chart->renderTo('chart_2');  // The #id of the div where to render the chart
        $ob2->chart->type('line');
        $ob2->title->text('Egresos Operacionales');
        $ob2->subtitle->text('Ultimos 6 meses');
        $ob2->yAxis->title(array('text'  => "Miles CLP"));
        $ob2->xAxis->categories($arr_gra_mes_op_cat);
        $ob2->plotOptions->line(array(
        'allowPointSelect'  => true,
        'dataLabels'    => array('enabled' => true),
        'numerixsymbols'  => false
        ));
        $ob2->series($series);
        
        
        
        return $this->render
                ('GestionCajaBundle:Default:dash.peso.index.html.twig', 
                array(
                    'res_eg_15'=> $arr_res_dia_op,
                    'res_eg_15_dias'=> $arr_res_dia_op_dias,
                    'arr_res_dia_nop'=> $arr_res_dia_nop,
                    'arr_res_dia_nop_dias'=> $arr_res_dia_nop_dias,
                    'arr_res_mes_op'=> $arr_res_mes_op,
                    'arr_res_mes_op_fechas'=> $arr_res_mes_op_fechas,
                    'arr_res_mes_nop'=> $arr_res_mes_nop,
                    'arr_res_mes_nop_fechas'=> $arr_res_mes_nop_fechas,
                    'chart_1' => $ob1,
                    'chart_2' => $ob2,
                    'saldo_actual_clp' => $saldo_actual_clp,
                    'saldo_actual_usd' => $saldo_actual_usd,
                    'tours_por_cobrar' => $tours_por_cobrar,
                    'mes_actual' => strtoupper(date('M')),
                    //'valor_usd' => $valor_dolar,
                    //'valor_euro' => $valor_euro
                ));
    }
           

        public function vercajaAction($id)
	{

            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
            'SELECT c.id, c.fecha, c.monto, c.nombre, c.tipo, o.concepto, c.nula, m.moneda, c.doc, c.hab, c.pax, c.usuario, c.obs, c.hora, m.id as idmoneda     
            FROM GestionCajaBundle:Caja c 
            inner JOIN GestionCajaBundle:CajaConcepto o
            with c.cajaconcepto_id = o.id
            inner JOIN GestionCajaBundle:Moneda m
            with c.moneda_id = m.id
            WHERE c.id = :id'
            )->setParameter('id', $id);
            
            $mov = $query->getResult();
            
            return $this->render
                ('GestionCajaBundle:Default:mov.html.twig', 
                array('mov'=>$mov)
                );
	}
        
        
	public function filterbuscarAction()
	{

        // VALOR DOLAR 
        //$misc = new MiscController();
        //$valor_dolar= $misc->obtieneValorDolar();
		
        $form = $this->get('form.factory')->create(new filtroBuscarCajasType());

        if ($this->get('request')->query->has($form->getName())) {
            // manually bind values from the request
            $form->submit($this->get('request')->query->get($form->getName()));         

            // initialize a query builder
            $filterBuilder = $this->get('doctrine.orm.entity_manager')
                ->getRepository('GestionCajaBundle:Caja')
                ->createQueryBuilder('e');

            $filterBuilder
                ->select('e.id, e.fecha, e.nombre, e.doc, e.pax, e.hab, e.monto,e.tipo, c.concepto, e.obs, e.moneda_id, e.nula')
                ->Join('GestionCajaBundle:CajaConcepto', 'c')
                ->where('e.cajaconcepto_id=c.id')
                ->orderBy('e.id', 'DESC');

            $qbUpdater = $this->get('lexik_form_filter.query_builder_updater');

            // build the query from the given form object
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);

            // now look at the DQL =)
            //var_dump($filterBuilder->getDql());

            $movs = $filterBuilder->getQuery();

            // Añadimos el paginador (En este caso el parámetro "1" es la página actual, y parámetro "10" es el número de páginas a mostrar)
            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
            $filterBuilder,
            $this->get('request')->query->get('page', 1),1000);


            return $this->render('GestionCajaBundle:Default:inf.buscar.caja.html.twig', array(
                //'boletas' => $boletas,
                'form' => $form->createView(),
                'movs' => $pagination,
                //'valor_usd'=>$valor_dolar
                        ));
        }
        
        //Datos NAVBAR
        $em = $this->getDoctrine()->getManager();
        $saldo_actual_clp = $em->getRepository('GestionCajaBundle:CajaCierre')
                     ->getSaldoActual(1);
        $saldo_actual_usd = $em->getRepository('GestionCajaBundle:CajaCierre')
                     ->getSaldoActual(2);  

        return $this->render('GestionCajaBundle:Default:inf.buscar.caja.html.twig', array(
            'form' => $form->createView(),
            'saldo_actual_clp'=>$saldo_actual_clp,
            'saldo_actual_usd'=>$saldo_actual_usd
            ));
    }	
}