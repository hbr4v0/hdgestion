<?php

namespace Gestion\TraspasosBundle\Controller;

use Gestion\TraspasosBundle\Entity\Traspasos;
use Gestion\CajaBundle\Entity\TraspasosTours;
use Gestion\CajaBundle\Entity\Tos;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Gestion\TraspasosBundle\Form\filtroInfTrnfFechaToType;
use Gestion\TraspasosBundle\Form\filtroInfFechaToType;
use Gestion\TraspasosBundle\Form\filtroInfFechaType;
use Symfony\Component\HttpFoundation\Response;

use Knp\Bundle\SnappyBundle\KnpSnappyBundle;

class InformesController extends Controller
{
        
	public function fecha_toAction()
	{
		
            $form = $this->get('form.factory')->create(new filtroInfFechaToType());

            if ($this->get('request')->query->has($form->getName())) {
                // manually bind values from the request
                $form->submit($this->get('request')->query->get($form->getName()));         

                // initialize a query builder
                $filterBuilder = $this->get('doctrine.orm.entity_manager')
                    ->getRepository('GestionTraspasosBundle:Traspasos')
                    ->createQueryBuilder('t');
                
                $to=$form->get('to_id')->getData();
                $res_operador=$to->getOperador();
                $rango_fecha=$form->get('fecha_traspaso')->getData();
                $res_desde= $rango_fecha['left_date'];
                $res_hasta=$rango_fecha['right_date'];
                $param_desde =$res_desde->format('Y-m-d');
                $param_hasta =$res_hasta->format('Y-m-d');
                
                $filterBuilder
                    ->select('t.id,t.to_id, t.fecha_traspaso, t.pax, t.n, t.costo,o.tour,t.pagado, t.nula, t.formapago ')
                    ->Join('GestionTraspasosBundle:TraspasosTours', 'o')
                    ->where('t.traspasotour_id=o.id');
                    //->where('t.to_id = ' . $to_id . '');
                    //->setParameter('to_id','2')    ;
                    //->orderBy('t.fecha_traspaso', 'ASC');

                $qbUpdater = $this->get('lexik_form_filter.query_builder_updater');

                // build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);

                // now look at the DQL =)
                //var_dump($filterBuilder->getDql())      ;

                $movs = $filterBuilder->getQuery();

                $paginator  = $this->get('knp_paginator');
                $pagination = $paginator->paginate(
                $filterBuilder,
                $this->get('request')->query->get('page', 1),1000);
                
                // CREA RESUMEN POR SERVICIO
                $desde = $res_desde->format('Y-m-d');
                $hasta = $res_hasta->format('Y-m-d');        
                $em = $this->getDoctrine()->getManager();
                $query = $em->createQuery(
                        "SELECT p.tour, Sum(t.n) AS n, Sum(t.costo) AS costo
                        FROM GestionTraspasosBundle:Traspasos t 
                        inner JOIN GestionTraspasosBundle:TraspasosTours p
                        with t.traspasotour_id = p.id
                        WHERE (t.fecha_traspaso Between '" . $desde . "' And '" . $hasta . "') AND (t.to_id=" . $to . " AND (t.nula=0))
                        GROUP BY p.tour"
                         );
                
                $res_servicio = $query->getResult();  

                return $this->render('GestionTraspasosBundle:Default:inf.tour.fecha.to.html.twig', array(
                    'form' => $form->createView(),
                    'movs' => $pagination,
                    'res_operador' => $res_operador,
                    'res_desde' => $res_desde,
                    'res_hasta' => $res_hasta,
                    'res_servicio' => $res_servicio,
                    'to_id'=>$to,
                    'desde'=> $desde,
                    'hasta'=> $hasta
                            ));
            }

                return $this->render('GestionTraspasosBundle:Default:inf.tour.fecha.to.html.twig', array(
                        'form' => $form->createView(),
                        ));
	}	
	public function tour_fecha_to_pdfAction($desde,$hasta,$to_id, $operador)
	{     

            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
            "SELECT t.id,t.to_id, t.fecha_traspaso, t.pax, t.n, t.costo,o.tour,t.pagado, t.nula 
             FROM GestionTraspasosBundle:Traspasos t 
             inner JOIN GestionTraspasosBundle:TraspasosTours o
             with t.traspasotour_id = o.id
             WHERE (t.fecha_traspaso BETWEEN '" . $desde . "' AND '" . $hasta . "') AND (t.to_id=" . $to_id . ") AND (t.nula=0)
             ORDER BY t.fecha_traspaso, t.pax ASC"
            );
            $movs = $query->getResult();   
                
              
            // CREA RESUMEN POR SERVICIO   
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
                    "SELECT p.tour, Sum(t.n) AS n, Sum(t.costo) AS costo
                    FROM GestionTraspasosBundle:Traspasos t 
                    inner JOIN GestionTraspasosBundle:TraspasosTours p
                    with t.traspasotour_id = p.id
                    WHERE (t.fecha_traspaso Between '" . $desde . "' And '" . $hasta . "') AND (t.to_id=" . $to_id . ") AND (t.nula=0)
                    GROUP BY p.tour"
                         );
                $res_servicio = $query->getResult();  
                               
                $html = $this->renderView('GestionTraspasosBundle:Default:inf.tour.fecha.to.pdf.html.twig', array(
                    'movs' => $movs,
                    'res_operador' => $operador,
                    'res_desde' => $desde,
                    'res_hasta' => $hasta,
                    'res_servicio' => $res_servicio,
                    'to_id'=>$to_id  
                            ));
                $file= "pago_". $operador . "_" . date("Y-m-d") .  ".pdf";
                return new Response(
                    $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                    200,
                    array(
                        'Content-Type'          => 'application/pdf',
                        'Content-Disposition'   => 'attachment; filename=' . $file . ''
                    )
                );

	}
        
        public function fechaAction()
	{
		
            $form = $this->get('form.factory')->create(new filtroInfFechaType());

            if ($this->get('request')->query->has($form->getName())) {
                // manually bind values from the request
                $form->submit($this->get('request')->query->get($form->getName()));         

                // initialize a query builder
                $filterBuilder = $this->get('doctrine.orm.entity_manager')
                    ->getRepository('GestionTraspasosBundle:Traspasos')
                    ->createQueryBuilder('t');
                
                $filterBuilder
                    ->select('t.id,t.to_id, t.fecha_traspaso, t.pax, t.n, t.costo,o.tour, t.cobrado, t pagado, t.nula  ')
                    ->Join('GestionTraspasosBundle:TraspasosTours', 'o')
                    //->where('t.traspasotour_id=o.id')
                    ->Join('GestionTraspasosBundle:Tos', 'r')
                    //->where('t.to_id=r.id')
                        //->setParameter('to_id','2')    ;
                    ->orderBy('t.fecha_traspaso', 'ASC');

                $qbUpdater = $this->get('lexik_form_filter.query_builder_updater');

                // build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);

                // now look at the DQL =)
                //var_dump($filterBuilder->getDql())      ;

                $movs = $filterBuilder->getQuery();
                
                $paginator  = $this->get('knp_paginator');
                $pagination = $paginator->paginate(
                $filterBuilder,
                $this->get('request')->query->get('page', 1),1000);

                return $this->render('GestionTraspasosBundle:Default:inf.fecha.html.twig', array(
                    //'boletas' => $boletas,
                    'form' => $form->createView(),
                    'movs' => $pagination
                            ));
            }
                    return $this->render('GestionTraspasosBundle:Default:inf.fecha.html.twig', array(
                                                    //'boletas' => $boletas,
                        'form' => $form->createView(),
                        ));
	}	

     
	public function trnf_fecha_toAction()
	{
		
            $form = $this->get('form.factory')->create(new filtroInfTrnfFechaToType());

            if ($this->get('request')->query->has($form->getName())) {
                // manually bind values from the request
                $form->submit($this->get('request')->query->get($form->getName()));         

                // initialize a query builder
                $filterBuilder = $this->get('doctrine.orm.entity_manager')
                    ->getRepository('GestionTraspasosBundle:Transfers')
                    ->createQueryBuilder('t');
                
                $op=$form->get('op_id')->getData();
                $res_operador=$op->getOperador();
                $rango_fecha=$form->get('fecha_transfer')->getData();
                $res_desde= $rango_fecha['left_date'];
                $res_hasta=$rango_fecha['right_date'];
                
                $filterBuilder
                    ->select('t.id,t.op_id, t.fecha_transfer, t.pax, t.n, s.servicio, t.nula ')
                    ->Join('GestionTraspasosBundle:TransferServicios', 's')
                    ->where('t.servicio_id=s.id');
                    //->where('t.to_id = ' . $to_id . '');
                    //->setParameter('to_id','2')    ;
                    //->orderBy('t.fecha_traspaso', 'ASC');

                $qbUpdater = $this->get('lexik_form_filter.query_builder_updater');

                // build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);

                // now look at the DQL =)
                //var_dump($filterBuilder->getDql())      ;

                $movs = $filterBuilder->getQuery();

                $paginator  = $this->get('knp_paginator');
                $pagination = $paginator->paginate(
                $filterBuilder,
                $this->get('request')->query->get('page', 1),1000);
                
                // CREA RESUMEN POR SERVICIO
                $desde = $res_desde->format('Y-m-d');
                $hasta = $res_hasta->format('Y-m-d');        
                $em = $this->getDoctrine()->getManager();
                $query = $em->createQuery(
                        "SELECT s.servicio, Sum(t.n) AS n 
                        FROM GestionTraspasosBundle:Transfers t 
                        inner JOIN GestionTraspasosBundle:TransferServicios s
                        with t.servicio_id = s.id
                        WHERE (t.fecha_transfer Between '" . $desde . "' And '" . $hasta . "') AND (t.op_id=" . $op . " AND (t.nula=0))
                        GROUP BY s.servicio"
                         );
                
                $res_servicio = $query->getResult();  
                
                return $this->render('GestionTraspasosBundle:Default:inf.trnf.fecha.to.html.twig', array(
                    'form' => $form->createView(),
                    'movs' => $pagination,
                    'res_operador' => $res_operador,
                    'res_desde' => $res_desde,
                    'res_hasta' => $res_hasta,
                    'res_servicio' => $res_servicio
                            ));
            }

                return $this->render('GestionTraspasosBundle:Default:inf.trnf.fecha.to.html.twig', array(
                        'form' => $form->createView(),
                        ));
	}        
        
        
        
}