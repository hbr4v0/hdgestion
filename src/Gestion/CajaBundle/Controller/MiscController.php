<?php

namespace Gestion\CajaBundle\Controller;

use Gestion\CajaBundle\Entity\Caja;
use Gestion\CajaBundle\Entity\CajaRepository;
use Gestion\CajaBundle\Entity\CajaCierre;
use Gestion\CajaBundle\Entity\CajaCierreRepository;
use Gestion\CajaBundle\Entity\CajaConcepto;
use Gestion\CajaBundle\Entity\Moneda;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;

class MiscController extends Controller
{
    
    
    public function indexAction()
    {
            //return $this->render('GestionMainBundle:Default:index.html.twig', array('name' => $name));
    }

        
    public function obtieneValorDolar()
    {
        // VALOR DOLAR 
        $data = file_get_contents("http://codenova.cl/apps/indicadores/json");
        $items = json_decode($data);
        
        if (file_exists("http://codenova.cl/apps/indicadores/json"))
            {
        
            foreach ($items->items as $item)
                {
                    $label=$item->label;
                    $var1 = "D&oacute;lar Observado";
                    $var2 = $item->label;
                    $var2= substr($var2,0,22);
                    if (strcmp($var1, $var2) == 0){
                        $valor_dolar = $item->value;
                    }
                }
            }
        else 
            {
            $valor_dolar= 0;
            }
        return $valor_dolar;  
    }

        
    public function obtieneValorEuro()
    {
        // VALOR EURO
        $data = file_get_contents("http://codenova.cl/apps/indicadores/json");
        $items = json_decode($data);
        
        if (file_exists("http://codenova.cl/apps/indicadores/json"))
            {
        
            foreach ($items->items as $item)
                {
                    $label=$item->label;
                    $var1 = "Euro";
                    $var2 = $item->label;
                    $var2= substr($var2,0,22);
                    if (strcmp($var1, $var2) == 0){
                        $valor_euro = $item->value;
                    }
                }
            }
        else 
            {
            $valor_euro= 0;
            }
        return $valor_euro;  
    }  
}