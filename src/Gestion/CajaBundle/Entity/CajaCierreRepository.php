<?php 

namespace Gestion\CajaBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CajaCierreRepository extends EntityRepository
{

    public function ultimoCierre()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT MAX(c.cierre)
            	 FROM GestionCajaBundle:CajaCierre c'
            )->getResult();
    }
       
    public function getFechaCierre($cierre)
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT c.fecha
            	 FROM GestionCajaBundle:CajaCierre c
                 WHERE c.cierre = " . "'" . $cierre . "'"
            )->getResult();
        
    }

    public function getUsuarioCierre($cierre)
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT c.usuario
            	 FROM GestionCajaBundle:CajaCierre c
                 WHERE c.cierre = " . "'" . $cierre . "'"
            )->getResult();
    }    

    public function getSaldoCierrePeso($cierre)
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT c.saldo
            	 FROM GestionCajaBundle:CajaCierre c
                 WHERE c.cierre = " . "'" . $cierre . "' AND c.moneda_id=1"
            )->getResult();
    }  
    
    public function getSaldoCierreDolar($cierre)
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT c.saldo
            	 FROM GestionCajaBundle:CajaCierre c
                 WHERE c.cierre = " . "'" . $cierre . "' AND c.moneda_id=2"
            )->getResult();
    }  
    
    public function getEsFechaCerrada($fecha)
    {

        $ret= $this->getEntityManager()
            ->createQuery(
                'SELECT MAX(v.numero)
            	 FROM GestionMainBundle:CajaCierre v 
            	 WHERE v.doc_id= ' . $fecha
            )->getResult();
  
    }
    
    public function getSaldoActual($idmoneda)
    {
        
        $fecha_cierre = $this->getEntityManager()
            ->createQuery(
                'SELECT MAX(c.cierre)
            	 FROM GestionCajaBundle:CajaCierre c'
            )->getResult();
        $fecha_cierre = array_shift($fecha_cierre['0']);
        if ($idmoneda=='1'){
            $saldo_cierre = $this->getSaldoCierrePeso($fecha_cierre);
        }else{
            $saldo_cierre = $this->getSaldoCierreDolar($fecha_cierre);
        }

        $saldo_cierre= array_shift($saldo_cierre['0']);

        // Suma y resta mocimientos posteriores al ultimo cierre

        $query = $this->getEntityManager()
            ->createQuery(
                'SELECT c.id, c.fecha, c.monto, c.nombre, c.tipo, o.concepto, c.nula, m.moneda,m.id AS idmoneda, c.doc, c.hab, c.pax, c.obs 
                 FROM GestionCajaBundle:Caja c 
                 inner JOIN GestionCajaBundle:CajaConcepto o
                 with c.cajaconcepto_id = o.id
                 inner JOIN GestionCajaBundle:Moneda m
                 with c.moneda_id = m.id
                 WHERE c.fecha > :cierre AND (m.id=:idmoneda) 
                 ORDER BY c.id DESC'
            )
                ->setParameter('cierre', $fecha_cierre)
                ->setParameter('idmoneda', $idmoneda);
        $movs = $query->getResult();

        $ingresos = 0;
        $egresos = 0;
        
        foreach ($movs as $mov)
        {
            if ($mov['tipo']=='1'){
                $ingresos = $ingresos + $mov['monto'];
            }
            else {
                $egresos = $egresos + $mov['monto'];
            }
        }
        
        $saldo_actual = ($saldo_cierre + $ingresos - $egresos) ;
        
        return $saldo_actual;  
        
    } 
 
}