<?php

namespace Gestion\TraspasosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * traspasos
 *
 * @ORM\Table(name="traspasos", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Gestion\TraspasosBundle\Entity\TraspasosRepository")
 */

class Traspasos

{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
    * @var date
     *  @ORM\Column(name="fecha_ingreso", type="date", nullable=false)
     */
    private $fecha_ingreso;

    /**
    *  @var time
     *  @ORM\Column(name="hora_ingreso", type="time", nullable=true)
     */
    private $hora_ingreso;
    
     /**
    * @var string
     *  @ORM\Column(name="usuario", type="string", nullable=true)
     */
    private $usuario;
    
    /**
    * @var date
     *  @ORM\Column(name="fecha_traspaso", type="date", nullable=false)
     */
    private $fecha_traspaso;    

    /**
     * @var string
     * @ORM\Column(name="obs", type="string", nullable=true)
     */
    private $obs;    

    /**
     * @var string
     * @ORM\Column(name="formapago", type="string", nullable=true)
     */
    private $formapago;  
    
    /**
     * @var string
     *      * 
     *@ORM\Column(name="pax", type="string", nullable=true)
     */
    private $pax;

    /**
     * @var integer
     *      * 
     *@ORM\Column(name="n", type="integer", nullable=false)
     */
    private $n;

    /**
     * @var integer
     *      * 
     *@ORM\Column(name="to_id", type="integer", nullable=false)
     */
    private $to_id;

     /**
     * @var integer
     *      * 
     *@ORM\Column(name="cobrado", type="integer", nullable=false)
     */
    private $cobrado;
    
     /**
     * @var integer
     *       
     *@ORM\Column(name="traspasotour_id", type="integer", nullable=false)
     */
    private $traspasotour_id;

    /**
     * @var integer;
     *
     * @ORM\Column(name="nula", type="integer", nullable=false)
     */
    private $nula;    
    
    /**
     * @var integer;
     *
     * @ORM\Column(name="pagado", type="integer", nullable=false)
     */
    private $pagado;      
    
    
    /**
     * @var integer;
     *
     * @ORM\Column(name="hab", type="integer", nullable=false)
     */
    private $hab;      
 
    /**
     * @var integer;
     *
     * @ORM\Column(name="costo", type="integer", nullable=false)
     */
    private $costo;  

    /**
     * @var integer;
     *
     * @ORM\Column(name="pago_doc", type="integer", nullable=true)
     */
    private $pago_doc;
    
    /**
     * @var date
     *  @ORM\Column(name="fecha_pago", type="date", nullable=true)
     */
    private $fecha_pago;  
    
     /**
     * @var time
     *  @ORM\Column(name="hora_pago", type="time", nullable=true)
     */
    private $hora_pago; 
    
     /**
     * @var string
     *  @ORM\Column(name="user_pago", type="string", nullable=true)
     */
    private $user_pago;    
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pax
     *
     * @param string $pax
     *
     * @return Traspasos
     */
    public function setPax($pax)
    {
        $this->pax = $pax;

        return $this;
    }
    
    /**
     * Get pax
     *
     * @return string
     */
    public function getPax()
    {
        return $this->pax;
    }

     /**
     * Set fecha_ingreso
     *
     * @param date $fecha_ingreso
     *
     * @return date
     */
    public function setFechaIngreso($fecha_ingreso)
    {
        $this->fecha_ingreso = $fecha_ingreso;

        return $this;
    }

    /**
     * Get fecha_ingreso
     *
     * @return date
     */
    public function getFechaIngreso()
    {
        return $this->fecha_ingreso;
    }

     /**
     * Set fecha_traspaso
     *
     * @param date $fecha_traspaso
     *
     * @return date
     */
    public function setFechaTraspaso($fecha_traspaso)
    {
        $this->fecha_traspaso = $fecha_traspaso;

        return $this;
    }


    /**
     * Get fecha_traspaso
     *
     * @return date
     */
    public function getFechaTraspaso()
    {
        return $this->fecha_traspaso;
    }     
    
    /**
     * Get n
     *
     * @return string
     */
    public function getN()
    {
        return $this->n;
    } 
    
     /**
     * Set n
     *
     * @param string $n
     *
     * @return Traspasos
     */
    public function setN($n)
    {
        $this->n = $n;

        return $this;
    }

    /**
     * Get to_id
     *
     * @return string
     */
    public function getToId()
    {
        return $this->to_id;
    } 
    
     /**
     * Set to_id
     *
     * @param string $to_id
     *
     * @return Traspasos
     */
    public function setToId($to_id)
    {
        $this->to_id = $to_id;

        return $this;
    }  

    /**
     * Set obs
     *
     * @param string $obs
     *
     * @return Tos
     */
    public function setObs($obs)
    {
        $this->obs = $obs;

        return $this;
    }

    /**
     * Get obs
     *
     * @return string
     */
    public function getObs()
    {
        return $this->obs;
    } 

    /**
     * Set traspasotour_id
     *
     * @param integer $traspasotour_id
     *
     * @return TraspasosTours
     */
    public function setTraspasotourId($traspasotour_id)
    {
        $this->traspasotour_id = $traspasotour_id;

        return $this;
    }

    /**
     * Get traspasotour_id
     *
     * @return integer
     */
    public function getTraspasoTourId()
    {
        return $this->traspasotour_id;
    }   
    
    /**
     * Set hora_ingreso
     *
     * @param time $hora_ingreso
     *
     * @return time
     */
    public function setHoraingreso($hora_ingreso)
    {
        $this->hora_ingreso = $hora_ingreso;

        return $this;
    }

    /**
     * Get hora_ingreso
     *
     * @return time
     */
    public function getHoraingreso()
    {
        return $this->hora_ingreso;
    }       

    /**
     * Set usuario
     *
     * @param string $usuario
     *
     * @return Traspasos
     */
    
    
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string
     */
    public function getUsuario()
    {
        return $this->usuario;
    }   

    /**
     * Set formapago
     *
     * @param string $formapago
     *
     * @return Traspasos
     */

    public function setFormapago($formapago)
    {
        $this->formapago= $formapago;

        return $this;
    }

    /**
     * Get formapago
     *
     * @return string
     */
    public function getFormapago()
    {
        return $this->formapago;
    }     
    
    
    /**
     * Get nula
     *
     * @return integer
     */
    public function getNula()
    {
    	return $this->nula;
    }

    /**
     * Set nula
     *
     * @param integer $nula
     * @return integer
     */
    
    public function setNula($nula)
    {
    	$this->nula = $nula;
    
    	return $this;
    }  
 
        
    /**
     * Get cobrado
     *
     * @return integer
     */
    public function getCobrado()
    {
    	return $this->cobrado;
    }

    /**
     * Set cobrado
     *
     * @param integer $cobrado
     * @return integer
     */
    
    public function setCobrado($cobrado)
    {
    	$this->cobrado = $cobrado;
    
    	return $this;
    }  
    
            
    /**
     * Get costo
     *
     * @return integer
     */
    public function getCosto()
    {
    	return $this->costo;
    }

    /**
     * Set costo
     *
     * @param integer $costo
     * @return integer
     */
    
    public function setCosto($costo)
    {
    	$this->costo = $costo;
    
    	return $this;
    } 
    
               
    /**
     * Get hab
     *
     * @return integer
     */
    public function getHab()
    {
    	return $this->hab;
    }

    /**
     * Set hab
     *
     * @param integer $hab
     * @return integer
     */
    
    public function setHab($hab)
    {
    	$this->hab = $hab;
    
    	return $this;
    }  
 
    
                
    /**
     * Get pagado
     *
     * @return integer
     */
    public function getPagado()
    {
    	return $this->pagado;
    }

    /**
     * Set pagado
     *
     * @param integer $pagado
     * @return integer
     */
    
    public function setPagado($pagado)
    {
    	$this->pagado = $pagado;
    
    	return $this;
    } 
    
    
    /**
     * Get pago_doc
     *
     * @return integer
     */
    public function getPagoDoc()
    {
    	return $this->pago_doc;
    }

    /**
     * Set pago_doc
     *
     * @param integer $pago_doc
     * @return integer
     */
    
    public function setPagoDoc($pago_doc)
    {
    	$this->pago_doc = $pago_doc;
    
    	return $this;
    }   

     /**
     * Set fecha_pago
     *
     * @param date $fecha_pago
     *
     * @return date
     */
    public function setFechaPago($fecha_pago)
    {
        $this->fecha_pago = $fecha_pago;

        return $this;
    }

    /**
     * Get fecha_pago
     *
     * @return date
     */
    public function getFechaPago()
    {
        return $this->fecha_pago;
    }

     /**
     * Set hora_pago
     *
     * @param time $hora_pago
     *
     * @return time
     */
    public function setHoraPago($hora_pago)
    {
        $this->hora_pago = $hora_pago;

        return $this;
    }

    /**
     * Get hora_pago
     *
     * @return time
     */
    public function getHoraPago()
    {
        return $this->hora_pago;
    }

     /**
     * Set user_pago
     *
     * @param time $user_pago
     *
     * @return time
     */
    public function setUserPago($user_pago)
    {
        $this->user_pago = $user_pago;

        return $this;
    }

    /**
     * Get user_pago
     *
     * @return time
     */
    public function getUserPago()
    {
        return $this->user_pago;
    }

    
    /**
     * @ORM\OneToMany(targetEntity="Tos", mappedBy="traspaso")
     * @ORM\OneToMany(targetEntity="TraspasosTours", mappedBy="traspaso")
     */
    private $traspasos;
    
    public function __construct()
        
    {
        $this->traspasos = new ArrayCollection();
    }    
    
    
}


