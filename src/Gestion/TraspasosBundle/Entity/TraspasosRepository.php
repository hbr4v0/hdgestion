<?php 

namespace Gestion\TraspasosBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TraspasosRepository extends EntityRepository
{

    public function getToursPorCobrar($nmes)
    {
        
        
        
        $anio=date("Y");
        $dias_mes= date("d",(mktime(0,0,0,$nmes+1,1,$anio)-1));
        $dias=intval($dias_mes);
        $hasta= $anio . "-" . $nmes . "-" . $dias ;
        $desde= date('Y-m-01'); // first day of this month
        
        
        $arr= $this->getEntityManager()
            ->createQuery(
                "SELECT t.nula, t.pagado, Sum(t.cobrado) AS porpagar
                FROM GestionTraspasosBundle:Traspasos t
                WHERE t.fecha_traspaso Between '" . $desde . "' And '" . $hasta . "' 
                GROUP BY t.nula, t.pagado
                HAVING t.nula=0 AND t.pagado=0"    
                )
            //->setParameter('to_id', $to_id)
            //->setParameter('tour_id', $tour_id)    
            ->getResult();
        
        //intval(($arr['0']['porpagar']));
        
        if (isset($arr['0']['porpagar'])) 
            {
            $ret= intval($arr['0']['porpagar']);
        }
        else
            {
            $ret=0;
        }
        
        return $ret;
        
    }   
    
}