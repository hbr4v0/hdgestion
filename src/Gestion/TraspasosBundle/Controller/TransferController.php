<?php

namespace Gestion\TraspasosBundle\Controller;

use Gestion\TraspasosBundle\Entity\Transfers;
use Gestion\TraspasosBundle\Entity\TransferOps;
use Gestion\TraspasosBundle\Entity\TraspasosTours;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TransferController extends Controller
{ 
    
    public function indextransfersAction()   
    {
        $hoy = new \DateTime("now");
        $ayer = new \DateTime("now");
        $ayer->modify('-1 day');
        $m = new \DateTime("now");
        $m->modify('+1 day');
        $manana = $m->format("Y-m-d");
        
        $mes=date("m");
        $anio=date("Y");
        $desde= date('Y-m-01'); // first day of this month
        
        
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
        'SELECT t.id, t.fecha_transfer,t.vuelo,t.hora_transfer, t.hora_vuelo, t.hab, t.pax, t.n, t.nula, o.operador, p.servicio,t.obs  
         FROM GestionTraspasosBundle:Transfers t 
         inner JOIN GestionTraspasosBundle:TransferOps o
         with t.op_id = o.id
         inner JOIN GestionTraspasosBundle:TransferServicios p
         with t.servicio_id = p.id
         WHERE (t.fecha_transfer >= :hoy)  
         ORDER BY t.fecha_transfer DESC'
        )->setParameter('hoy', $desde);

        $movs = $query->getResult();       
        
        
        $hoy2 = date("Y-m-d");
        $query2 = $em->createQuery(
        'SELECT t.id, t.fecha_transfer,t.vuelo,   t.hora_transfer, t.hora_vuelo, t.hab, t.pax, t.n, t.nula, o.operador, p.servicio  
         FROM GestionTraspasosBundle:Transfers t 
         inner JOIN GestionTraspasosBundle:TransferOps o
         with t.op_id = o.id
         inner JOIN GestionTraspasosBundle:TransferServicios p
         with t.servicio_id = p.id
         WHERE (t.fecha_transfer =:hoy) AND (t.nula=0) 
         ORDER BY t.pax, t.fecha_transfer ASC'
        )->setParameter('hoy', $hoy2);

        $movs_hoy = $query2->getResult();        
        
        $query3 = $em->createQuery(
        'SELECT t.id, t.fecha_transfer,t.vuelo,   t.hora_transfer, t.hora_vuelo, t.hab, t.pax, t.n, t.nula, o.operador, p.servicio  
         FROM GestionTraspasosBundle:Transfers t 
         inner JOIN GestionTraspasosBundle:TransferOps o
         with t.op_id = o.id
         inner JOIN GestionTraspasosBundle:TransferServicios p
         with t.servicio_id = p.id
         WHERE (t.fecha_transfer =:manana) AND (t.nula=0) 
         ORDER BY t.pax, t.fecha_transfer ASC'
        )->setParameter('manana', $manana);

        $movs_manana = $query3->getResult();
        
        // HISTORICO VENTA TRANSFERS
        $m = new \DateTime("now");
        for ($i = 0; $i <= 4; $i++)
            {
                if ( $i > 0 ){
                    $m->modify('-1 month');
                }
                $mes=$m->format("M") . "/" . $m->format("y");
                $nmes=$m->format("m");
                
                $fecha1 = $m->format('Y') . "-" . $m->format('m') . "-01";
                $anio=date("Y");
                $dias_mes= date("d",(mktime(0,0,0,$nmes+1,1,$anio)-1));
                $dias=intval($dias_mes);
                
                $fecha2 = $m->format('Y') . "-" . $m->format('m') . "-" . $dias;
                
                
                $q=$em->createQuery("SELECT Sum(t.n) AS N
                                    FROM GestionTraspasosBundle:Transfers t 
                                    WHERE (t.fecha_transfer Between '" . $fecha1 . "' And '" . $fecha2 . "') AND (t.servicio_id=1) AND (t.nula=0)"
                                    );
                $result=$q->getResult();
                $trnfin= array_shift($result['0']);
 
                $q=$em->createQuery("SELECT Sum(t.n) AS N
                                    FROM GestionTraspasosBundle:Transfers t 
                                    WHERE (t.fecha_transfer Between '" . $fecha1 . "' And '" . $fecha2 . "') AND (t.servicio_id=2) AND (t.nula=0)"
                                    );
                $result=$q->getResult();
                $trnfout= array_shift($result['0']);
                
                $trnf=$trnfin + $trnfout;
                
                $ret_trnf[$i] = array('mes'=>$mes, 'trnfin'=>$trnfin, 'trnfout'=>$trnfout, 'total'=>$trnf);
            }        
        
            // % Variacion
            
            $tot1 = $ret_trnf[0]['total'];
            $tot2 = $ret_trnf[1]['total'];
            $tot3 = $ret_trnf[2]['total'];
            $tot4 = $ret_trnf[3]['total'];
            $tot5 = $ret_trnf[4]['total'];
            
            if ($tot2 == '0'){
                $var1 = 100;
            }
            else {
                $var1 = (100-(($tot1*100)/$tot2))*-1;
            }
 
            if ($tot3 == '0'){
                $var2 = 0;
            }
            else {
                $var2 = (100-(($tot2*100)/$tot3))*-1;
            }
            
            if ($tot4 == '0'){
                $var3 = 0;
            }
            else {
                $var3 = (100-(($tot3*100)/$tot4))*-1;
            }
            
            if ($tot5 == '0'){
                $var4 = 0;
            }
            else {
                $var4 = (100-(($tot4*100)/$tot5))*-1;
            }           

            $var5 = 0;

            $ret_trnf[0]['variacion']=$var1;
            $ret_trnf[1]['variacion']=$var2;
            $ret_trnf[2]['variacion']=$var3;
            $ret_trnf[3]['variacion']=$var4;
            $ret_trnf[4]['variacion']=$var5; 
        
        //RESUMEN MENSUAL
        $mes=date("m");
        $anio=date("Y");
        $dias_mes= date("d",(mktime(0,0,0,$mes+1,1,$anio)-1));
        $dias=intval($dias_mes);
        $hasta= $anio . "-" . $mes . "-" . $dias ;
        $desde= date('Y-m-01'); // first day of this month
        
        $query8 = $em->createQuery(
        "SELECT o.operador, p.servicio, Sum(t.n) AS N
         FROM GestionTraspasosBundle:Transfers t 
         inner JOIN GestionTraspasosBundle:TransferOps o
         with t.op_id = o.id
         inner JOIN GestionTraspasosBundle:TransferServicios p
         with t.servicio_id = p.id
         WHERE t.fecha_transfer Between '" . $desde . "' AND '" . $hasta . "' AND t.nula=0
         GROUP BY o.operador, p.servicio, t.nula
         HAVING t.nula=0
         ORDER BY p.servicio");
        $trnf_resumen_mes = $query8->getResult();  
        
        return $this->render
                ('GestionTraspasosBundle:Default:index.transfers.html.twig', 
                array(
                    'movs'=>$movs,
                    'movs_hoy'=>$movs_hoy,
                    'hoy'=>$hoy,
                    'manana'=>$manana,
                    'trnf_resumen_mes'=>$trnf_resumen_mes,
                    'trnf_historico_vtas'=>$ret_trnf,
                    'movs_manana'=>$movs_manana)
                    );
    }    
        
    
    public function addtransferAction(Request $request)
            
    {
            $transfer = new Transfers();
            
            $form = $this->createFormBuilder($transfer)
            ->add('fecha_transfer', 'genemu_jquerydate',array('widget'  => 'single_text',
                      'attr' => array('class' => 'form-control','format'   => 'y-MM-dd', 
                      )))
            ->add('hora_transfer', 'time',
                    array('widget'  => 'choice',
                          'input' =>'datetime',
                          'attr' => array('class' => 'form-control', 
                      )))
            ->add('hora_vuelo', 'time',
                    array('widget'  => 'choice',
                          'input' =>'datetime',
                          'attr' => array('class' => 'form-control', 
                      )))
            ->add('pax', 'text',array('attr' => array('class' => 'form-control', )  ))
            ->add('n','integer',array( 'attr' => array('class' => 'form-control','placeholder' => '0','required' => 'true')))
            ->add('op_id', 'genemu_jqueryselect2_entity', array(
                'class' => 'GestionTraspasosBundle:TransferOps',
                'property' => 'operador',
                'attr' => array('class' => 'form-control'),
                'data'=> '2',
                'required' => 'true',
                'empty_value'=>'Seleccione...',))                            
            ->add('servicio_id', 'genemu_jqueryselect2_entity', array(
                    'class' => 'GestionTraspasosBundle:TransferServicios',
                    'property' => 'servicio',
                'attr' => array('class' => 'form-control'),
                'required' => 'true',
                'empty_value'=>'Seleccione...',))  
            ->add('obs','text',array( 'attr' => array('class' => 'form-control','required' => false,'empty_value'=>'null')))
            ->add('vuelo','text',array( 'attr' => array('class' => 'form-control','required' => true,'empty_value'=>'null')))
            ->add('hab', 'genemu_jqueryselect2_choice', array(
                            'choices' => array(
                                            '0' => '0',
                                            '1' => '1',
                                            '2' => '2',
                                            '3' => '3',
                                            '4' => '4',
                                            '5' => '5',
                                            '6' => '6',
                                            '7' => '7',
                                            '8' => '8',
                                            '9' => '9'),
                            'empty_value'=>'Seleccione...',
                            'attr' => array('class' => 'form-control')))            
            ->add('Ingresar','submit',array( 'attr' => array('class' => 'btn btn-info')))
                    
            ->getform();

            $form->handleRequest($request);

            if ($form->isValid()) {
                
                $em = $this->getDoctrine()->getManager();
                $transfer->setFechaIngreso(new \DateTime("now"));    
                $transfer->setHoraingreso(new \DateTime("now")); 
                //Capitaliza nombre
                $pax=$form["pax"]->getData(); 
                $cpax = ucwords(strtolower($pax));
                $transfer->setPax($cpax);
                $transfer->setUsuario($this->getUser());
                $transfer->setNula(0);
                
                $em= $this->getDoctrine()->getManager();
                $em->persist($transfer);
                $em->flush();

                return $this->redirect($this->generateUrl('gestion_traspasos_transfers'));
            }

            return $this->render
                    ('GestionTraspasosBundle:Default:traspasos.addtransfer.html.twig', 
                    array("form"=>$form->createview(),
                        )
                    );
    } 

    public function editartransferAction($id,Request $request)
            
    {
    
        $em = $this->getDoctrine()->getEntityManager();
        $transfer = $em->getRepository('GestionTraspasosBundle:Transfers')->find($id);

        if (!$transfer) {
            throw $this->createNotFoundException('Registro inexistente...');
        }
        
        $to_id = $transfer->getOpId();
        $to = $em->getRepository('GestionTraspasosBundle:TransferOps')->find($to_id);
        $operador=$to->getOperador();            

        $servicio_id = $transfer->getServicioId();
        $serv = $em->getRepository('GestionTraspasosBundle:TransferServicios')->find($servicio_id);
        $servicio=$serv->getServicio();          
        
        $form = $this->createFormBuilder($transfer)
            ->add('fecha_transfer', 'genemu_jquerydate',array('widget'  => 'single_text',
                      'attr' => array('class' => 'form-control','format'   => 'y-MM-dd', 
                      )))
            ->add('hora_vuelo', 'time',
                    array('widget'  => 'choice',
                          'input' =>'datetime',
                          'attr' => array('class' => 'form-control', 
                      )))
                ->add('hora_transfer', 'time',
                    array('widget'  => 'choice',
                          'input' =>'datetime',
                          'attr' => array('class' => 'form-control', 
                      )))
            ->add('pax', 'text',array('attr' => array('class' => 'form-control', )  ))
            ->add('n','integer',array( 'attr' => array('class' => 'form-control','placeholder' => '0','required' => 'true')))
            ->add('obs','text',array( 'attr' => array('class' => 'form-control','required' => false,'empty_value'=>'null')))
            ->add('vuelo','text',array( 'attr' => array('class' => 'form-control','required' => true,'empty_value'=>'null')))
            ->add('hab', 'genemu_jqueryselect2_choice', array(
                            'choices' => array(
                                            '0' => '0',
                                            '1' => '1',
                                            '2' => '2',
                                            '3' => '3',
                                            '4' => '4',
                                            '5' => '5',
                                            '6' => '6',
                                            '7' => '7',
                                            '8' => '8',
                                            '9' => '9'),
                            'empty_value'=>'Seleccione...',
                            'attr' => array('class' => 'form-control')))            
            ->add('Guardar','submit',array( 'attr' => array('class' => 'btn btn-info')))
        ->getform();

        $form->handleRequest($request);

        if ($form->isValid()) {
          $em->flush();
          return $this->redirect($this->generateUrl('gestion_traspasos_transfers'));
        }

        return $this->render
                ('GestionTraspasosBundle:Default:traspasos.edittransfer.html.twig', 
                array("form"=>$form->createview(),
                      "operador"=>$operador,
                      "servicio"=>$servicio,
                    )
                );
    }            
    
    
    public function anulartransferAction($id)
	{
            $em = $this->getDoctrine()->getEntityManager();
            $transfer = $em->getRepository('GestionTraspasosBundle:Transfers')->find($id);
            $transfer->setNula(1);
            $em->persist($transfer);
            $flush = $em->flush();
            if ($flush == null) {
                    echo "Transfer anulado correctamente";
            } else {
                    echo "El transfer no se ha anulado";
            }
            return $this->redirect($this->generateUrl('gestion_traspasos_transfers'));
	}

    public function habilitartransferAction($id)
	{
            $em = $this->getDoctrine()->getEntityManager();
            $transfer = $em->getRepository('GestionTraspasosBundle:Transfers')->find($id);
            $transfer->setNula(0);
            $em->persist($transfer);
            $flush = $em->flush();
            if ($flush == null) {
                    echo "Transfer habilitada correctamente";
            } else {
                    echo "El transfer no se ha habilitar";
            }
            return $this->redirect($this->generateUrl('gestion_traspasos_transfers'));
	}

}
