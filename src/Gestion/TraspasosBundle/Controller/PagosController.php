<?php

namespace Gestion\TraspasosBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Gestion\TraspasosBundle\Entity\Traspasos;
use Gestion\TraspasosBundle\Entity\Tos;
use Gestion\TraspasosBundle\Entity\TraspasosTours;
use Gestion\CajaBundle\Entity\Caja;
use Gestion\CajaBundle\Entity\CajaConcepto;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ob\HighchartsBundle\Highcharts\Highchart;

class PagosController extends Controller
{

    public function indexAction(Request $request)   
    {
        
        $hoy = new \DateTime("now");
        $ayer = new \DateTime("now");
        $ayer->modify('-1 day');
        $m = new \DateTime("now");
        $m->modify('+1 day');
        $manana = $m->format("Y-m-d");
        
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
        'SELECT t.id, t.fecha_traspaso,   t.costo, t.hab, t.pax, t.n, t.cobrado, t.nula, t.pagado, o.operador, p.tour, t.obs   
         FROM GestionTraspasosBundle:Traspasos t 
         inner JOIN GestionTraspasosBundle:Tos o
         with t.to_id = o.id
         inner JOIN GestionTraspasosBundle:TraspasosTours p
         with t.traspasotour_id = p.id
         WHERE (t.pagado=0 AND t.nula=0) 
         ORDER BY t.fecha_traspaso, t.pax ASC'
        );

        $movs = $query->getResult();  
        
        $formBuilder = $this->createFormBuilder();
        
        $formBuilder->add('Siguiente', SubmitType::class,array( 'attr' => array('class' => 'btn btn-info','class' => 'form-control')));

        foreach($movs as $mov){
            $formBuilder
                ->add($mov['id'],CheckboxType::class,
                        array(
                            'label'=>$mov['id'],
                            'required'=>false,
                            'attr' => array('class' => 'form-control')));
        }
        
        $formBuilder->add('formapago', 'genemu_jqueryselect2_choice', array(
            'choices' => array(
                            'EFECTIVO' => 'EFECTIVO',
                            'TARJETA' => 'TARJETA',),
            'empty_value'=>'Seleccione...',
            'attr' => array('class' => 'form-control',
                            'onChange' => 'cambiaFormaPago()')));
        $formBuilder->add('pago_doc', 'text',array('attr' => array('class' => 'form-control', 
                                                                       'style' => 'width: 100px')  ));
        $form = $formBuilder->getForm();
        
        
        $form->handleRequest($request);

        if($form->isValid()){
            
        }
        $data = $form->getData();

        //$fecha_caja= $form["fecha"]->getData();
        $i = 0;
        $total=0;
        foreach($movs as $mov){
            $bool= $form[$mov['id']]->getData();

            if ( $bool == true)
            {
                $ids[$i]['id'] = $mov['id'];
                $ids[$i]['fecha_traspaso'] = $mov['fecha_traspaso'];
                $ids[$i]['pax'] = $mov['pax'];
                $ids[$i]['n'] = $mov['n'];
                $ids[$i]['tour'] = $mov['tour'];
                $ids[$i]['operador'] = $mov['operador'];
                $ids[$i]['cobrado'] = $mov['cobrado'];
                $total = $total + $mov['cobrado'];

                $ids_vector[$i] = $mov['id'];
            }
            $i= $i + 1;
            }
            
            if (isset($ids)){
                $n = count($ids);
                $ids_str = implode("','", $ids_vector);
                
                $forma_pago = $form["formapago"]->getData();
                $doc_pago = $form["pago_doc"]->getData();
                
                
                return $this->render
                       ('GestionTraspasosBundle:Default:traspasos.pagotours-2.html.twig', 
                       array(
                           "ids"=>$ids,
                           "total"=>$total,
                           "n"=>$n,
                           "ids_str"=>$ids_str,
                           "forma_pago"=>$forma_pago,
                           "doc_pago"=>$doc_pago,
                           "form"=>$form->createview()
                            ));               
            }else{
                $n = 0;
                $ids_str = 0;
            }
            
        return $this->render
                ('GestionTraspasosBundle:Default:traspasos.pagotours-1.html.twig', 
                array(
                    "movs"=>$movs,
                    "form"=>$form->createview())
                
                    );
    }

    public function pagartourAction($id,Request $request)
            
    {
    
        $em = $this->getDoctrine()->getEntityManager();
        $traspaso = $em->getRepository('GestionTraspasosBundle:Traspasos')->find($id);

        if (!$traspaso) {
            throw $this->createNotFoundException('Registro inexistente...');
        }
        
        $cobrado = $traspaso->getCobrado();
        $pax = $traspaso->getPax();
        
        $tour_id = $traspaso->getTraspasoTourId();
        $t = $em->getRepository('GestionTraspasosBundle:TraspasosTours')->find($tour_id);
        $tour = $t->getTour();
 
        $to_id = $traspaso->getToId();
        $t = $em->getRepository('GestionTraspasosBundle:Tos')->find($to_id);
        $to = $t->getOperador();       
        
        $form = $this->createFormBuilder($traspaso)
         ->add('formapago', 'genemu_jqueryselect2_choice', array(
                        'choices' => array(
                                        'Efectivo $' => 'Efectivo $',
                                        'Efectivo USD' => 'Efectivo USD',
                                        'Tarjeta Credito $' => 'Tarjeta Credito $',
                                        'Tarjeta Credito USD' => 'Tarjeta Credito USD'),
                        'empty_value'=>'Seleccione',
                        'attr' => array('class' => 'form-control')))
        ->add('pago_doc', 'text',array('attr' => array('class' => 'form-control', )  ))
        ->add('Pagar','submit',array( 'attr' => array('class' => 'btn btn-info')))

        ->getform();

        $form->handleRequest($request);

        if ($form->isValid()) {
          $traspaso->setPagado(1);  
          $em->flush();
          return $this->redirect($this->generateUrl('gestion_traspasos_tours'));
        }

        return $this->render
                ('GestionTraspasosBundle:Default:traspasos.pagartour.html.twig', 
                array("form"=>$form->createview(),
                      "pax"=>$pax,
                      "tour"=>$tour,
                      "to"=>$to,
                      "cobrado"=>$cobrado,
                      "id"=>$id  ,
                    )
                );
    }   
     
    public function pagartour2Action($ids_str, $forma_pago, $doc_pago, Request $request)
            
    {
        // Paga Tours
        
        var_dump($ids_str);
        $ids = explode("','", $ids_str);
        var_dump($ids);
        
        for($i=0;$i<count($ids);$i++) {
            
            $em1 = $this->getDoctrine()->getEntityManager();
            $tour = $em1->getRepository('GestionTraspasosBundle:Traspasos')->find($ids[$i]); 
            $tour->setPagado(1);
            $tour->setFormapago($forma_pago);
            $tour->setPagoDoc($doc_pago);
            $tour->setFechapago(new \DateTime("now"));
            $tour->setHorapago(new \DateTime("now"));
            $tour->setUserpago($this->getUser());
            $em1->flush();
            
            // Ingresa ingresos de caja a modulo Caja Solo si es pago efectivo
            if ($forma_pago=="EFECTIVO")
                {
                $caja = new Caja();
                $caja->setFecha(new \DateTime("now"));   
                $caja->setNombre($this->getUser());
                $caja->setUsuario($this->getUser());
                $caja->setMonto($tour->getCobrado());
                $caja->setTipo('1');
                $caja->setCajaconceptoId(4);
                $caja->setMonedaId(1);
                $caja->setNula(0);
                
                $caja->setObs("Pago tour id: " . $ids[$i] );
                
                //$obs= "&lt;a href=&quot;/gestion/web/app_dev.php/traspasos/ver/" . $ids[$i] .  "&quot;&gt;" . $ids[$i] . "</a>" ;
                
                //$obs="<a href="{{ path('gestion_traspasos_ver', { 'id' : pago.id }) }}">{{pago.id}}</a>";
                //$caja->setObs($obs);
                
                $caja->setHab($tour->getHab());
                $caja->setDoc($doc_pago);
                $caja->setHora(new \DateTime("now"));
                $caja->setNula(0);
                
                //Capitaliza nombre
                $pax = $tour->getPax(); 
                $cpax = ucwords(strtolower($pax));
                $caja->setPax($cpax);
                $em2= $this->getDoctrine()->getManager();
                $em2->persist($caja);
                $em2->flush();
                } // END IF
        } // END FOR
        
        return $this->redirect($this->generateUrl('gestion_traspasos_tours'));
    } // END FUNCTION
}
