<?php

namespace Gestion\CajaBundle\Controller;

use Gestion\CajaBundle\Entity\Caja;
use Gestion\CajaBundle\Entity\CajaRepository;
use Gestion\CajaBundle\Entity\CajaConcepto;
use Gestion\CajaBundle\Entity\CajaConceptoCateg;
use Gestion\CajaBundle\Controller\MiscController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ConfigController extends Controller
{
    
    public function indexAction()
    {
            //return $this->render('GestionMainBundle:Default:index.html.twig', array('name' => $name));
    }
        
    public function configegresosAction()
    {
        // VALOR DOLAR 
        $misc = new MiscController();
        $valor_dolar= $misc->obtieneValorDolar();
        
        // VECTOR CATEGORIAS DE CONCEPTOS DE GASTO
        
        $em = $this->getDoctrine()->getManager();
        
        $query = $em->createQuery(
        'SELECT c.id, c.categoria, c.operacional     
        FROM GestionCajaBundle:CajaConceptoCateg c 
        WHERE c.tipo = :tipo 
        ORDER BY c.categoria ASC'
        )->setParameter('tipo', '2'); // Egresos
        
        $cats = $query->getArrayResult();
        
        // Conceptos asociados a cada CAT (mete la info en array cats)
        $i=0;
        foreach ($cats as $cat) {
            $query = $em->createQuery(
            'SELECT c.id       
            FROM GestionCajaBundle:CajaConcepto c 
            WHERE c.cat_id = :cat_id'
            )->setParameter('cat_id', $cat['id']);     

            $tmp = $query->getArrayResult();
            
            $cats[$i]['conceptos'] = count($tmp);
            
            $i=$i+1;
        } 

        $query = $em->createQuery(
        'SELECT c.id, c.cat_id, c.tipo, c.concepto,c.nulo, t.tipo,t.categoria      
        FROM GestionCajaBundle:CajaConcepto c 
        inner JOIN GestionCajaBundle:CajaConceptoCateg t
        with c.cat_id = t.id
        WHERE t.tipo = :tipo 
        ORDER BY t.categoria ASC'
        )->setParameter('tipo', '2'); //      
        
        $conceptos = $query->getArrayResult();
        
        //Datos NAVBAR
        $saldo_actual_clp = $em->getRepository('GestionCajaBundle:CajaCierre')
                     ->getSaldoActual(1);
        $saldo_actual_usd = $em->getRepository('GestionCajaBundle:CajaCierre')
                     ->getSaldoActual(2);  
        

        return $this->render
                ('GestionCajaBundle:Default:config.egresos.html.twig', 
                array(
                    //'valor_usd'=>$valor_dolar,
                    'categorias'=>$cats,
                    'conceptos'=>$conceptos,
                    'saldo_actual_clp'=>$saldo_actual_clp,
                    'saldo_actual_usd'=>$saldo_actual_usd
                )
                );
    }
    
    public function editconceptoAction($id,Request $request)
            
    {
        // VALOR DOLAR 
        $misc = new MiscController();
        $valor_dolar= $misc->obtieneValorDolar();
    
        $em = $this->getDoctrine()->getEntityManager();
        $concepto = $em->getRepository('GestionCajaBundle:CajaConcepto')->find($id);

        if (!$concepto) {
            throw $this->createNotFoundException('Registro inexistente...');
        }         
        
        // Valor campo nulo y categoria
        $nulo=$concepto->getNulo();
        $cat_id=$concepto->getCatId();
        $tmp = $em->getRepository('GestionCajaBundle:CajaConceptoCateg')->find($cat_id);
        $categoria=$tmp->getCategoria();  
        
        $form = $this->createFormBuilder($concepto)
            ->add('cat_id', 'genemu_jqueryselect2_entity', array(
                'class' => 'GestionCajaBundle:CajaConceptoCateg',
                'property' => 'categoria',
                'attr' => array('class' => 'form-control'),
                'data'=>$em->getReference('GestionCajaBundle:CajaConceptoCateg',$cat_id)   ,
                'required' => 'true',
                ))  
            ->add('concepto', 'text',array('attr' => array('class' => 'form-control', )  ))  
            ->add('nulo', 'genemu_jqueryselect2_choice', array(
                'choices' => array(
                                '0' => 'Activo',
                                '1' => 'Nulo'),
                
                'data'=>$nulo,
                'attr' => array('class' => 'form-control')))   
            ->add('Guardar','submit',array( 'attr' => array('class' => 'btn btn-info')))
        ->getform();

        $form->handleRequest($request);

        if ($form->isValid()) {
          $em->flush();
          return $this->redirect($this->generateUrl('gestion_caja_config_egresos'));
        }

        return $this->render
                ('GestionCajaBundle:Default:config.egresos.editconcepto.html.twig', 
                array("form"=>$form->createview(),
                    'valor_usd'=>$valor_dolar,

                    )
                );
    }  
    
    public function addcategresoAction(Request $request)
            
    {
        // VALOR DOLAR 
        $misc = new MiscController();
        $valor_dolar= $misc->obtieneValorDolar();
    
        $categoria = new \Gestion\CajaBundle\Entity\CajaConceptoCateg();
        
        $form = $this->createFormBuilder($categoria)
            ->add('categoria', 'text',array('attr' => array('class' => 'form-control', )  ))  
            ->add('operacional', 'genemu_jqueryselect2_choice', array(
                'choices' => array(
                               '1' => 'Operacional',
                               '2' => 'No Operacional'),
               'empty_value'=>'Seleccione',
               'attr' => array('class' => 'form-control')))
            ->add('Guardar','submit',array( 'attr' => array('class' => 'btn btn-info')))
        ->getform();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $categoria->setTipo('2');
            $em= $this->getDoctrine()->getManager();
            $em->persist($categoria);
            $em->flush();
          return $this->redirect($this->generateUrl('gestion_caja_config_egresos'));
        }

        return $this->render
                ('GestionCajaBundle:Default:config.egresos.addcategoria.html.twig', 
                array("form"=>$form->createview(),
                    'valor_usd'=>$valor_dolar,

                    )
                );
    }     

    public function delcategresoAction($id)
            
    {
        $em = $this->getDoctrine()->getEntityManager();
        $cat = $em->getRepository('GestionCajaBundle:CajaConceptoCateg')->find($id);
        $em->remove($cat);
        $flush = $em->flush();
        if ($flush == null) {
                echo "Categoria eliminada correctamente";
        } else {
                echo "La Categoria no se ha eliminado";
        }
        return $this->redirect($this->generateUrl('gestion_caja_config_egresos'));
    }
    
    public function anularconceptoAction($id)
            
    {
    
        $em = $this->getDoctrine()->getEntityManager();
        $concepto = $em->getRepository('GestionCajaBundle:CajaConcepto')->find($id);
        $concepto->setNulo('1');
        $flush = $em->flush();
        if ($flush == null) {
                echo "Concepto anulado correctamente";
        } else {
                echo "El concepto no se ha anulado";
        }
        return $this->redirect($this->generateUrl('gestion_caja_config_egresos'));
        
    }    
    
    public function desanularconceptoAction($id)
            
    {
    
        $em = $this->getDoctrine()->getEntityManager();
        $concepto = $em->getRepository('GestionCajaBundle:CajaConcepto')->find($id);
        $concepto->setNulo('0');
        $flush = $em->flush();
        if ($flush == null) {
                echo "Concepto habilitado correctamente";
        } else {
                echo "El concepto no se ha habilitado";
        }
        return $this->redirect($this->generateUrl('gestion_caja_config_egresos'));
        
    }     
    
    
    
    
    
    
}
