<?php

namespace Gestion\TraspasosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * transfers
 *
 * @ORM\Table(name="transfers", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 */

class Transfers

{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
    * @var date
     *  @ORM\Column(name="fecha_ingreso", type="date", nullable=false)
     */
    private $fecha_ingreso;

    /**
    *  @var time
     *  @ORM\Column(name="hora_ingreso", type="time", nullable=true)
     */
    private $hora_ingreso;
    
     /**
    * @var string
     *  @ORM\Column(name="usuario", type="string", nullable=true)
     */
    private $usuario;
    
    /**
    * @var date
     *  @ORM\Column(name="fecha_transfer", type="date", nullable=false)
     */
    private $fecha_transfer;    

    /**
    * @var time
     *@ORM\Column(name="hora_transfer", type="time", nullable=false)
     */
    private $hora_transfer;
    
    /**
    * @var time
     *@ORM\Column(name="hora_vuelo", type="time", nullable=false)
     */
    private $hora_vuelo;    
    
    /**
     * @var string
     * @ORM\Column(name="obs", type="string", nullable=true)
     */
    private $obs;    
    
    /**
     * @var string
     *      * 
     *@ORM\Column(name="pax", type="string", nullable=true)
     */
    private $pax;

    /**
     * @var integer
     *      * 
     *@ORM\Column(name="n", type="integer", nullable=false)
     */
    private $n;

    /**
     * @var integer
     *      * 
     *@ORM\Column(name="op_id", type="integer", nullable=false)
     */
    private $op_id;
    
     /**
     * @var integer
     *       
     *@ORM\Column(name="servicio_id", type="integer", nullable=false)
     */
    private $servicio_id;

    /**
     * @var integer;
     *
     * @ORM\Column(name="nula", type="integer", nullable=false)
     */
    private $nula;     
    
    /**
     * @var integer;
     *
     * @ORM\Column(name="hab", type="integer", nullable=false)
     */
    private $hab;      

    /**
     * @var string;
     *
     * @ORM\Column(name="vuelo", type="string", nullable=false)
     */
    
    private $vuelo;  
    /**
     * Get id
     *
     * @return integer
     */
    
    
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pax
     *
     * @param string $pax
     *
     * @return Traspasos
     */
    public function setPax($pax)
    {
        $this->pax = $pax;

        return $this;
    }

    /**
     * Get pax
     *
     * @return string
     */
    public function getPax()
    {
        return $this->pax;
    }

     /**
     * Set fecha_ingreso
     *
     * @param date $fecha_ingreso
     *
     * @return date
     */
    public function setFechaIngreso($fecha_ingreso)
    {
        $this->fecha_ingreso = $fecha_ingreso;

        return $this;
    }

    /**
     * Get fecha_ingreso
     *
     * @return date
     */
    public function getFechaIngreso()
    {
        return $this->fecha_ingreso;
    }

     /**
     * Set fecha_transfer
     *
     * @param date $fecha_transfer
     *
     * @return date
     */
    public function setFechaTransfer($fecha_transfer)
    {
        $this->fecha_transfer = $fecha_transfer;

        return $this;
    }


    /**
     * Get fecha_transfer
     *
     * @return date
     */
    public function getFechaTransfer()
    {
        return $this->fecha_transfer;
    }     
    
    /**
     * Get n
     *
     * @return string
     */
    public function getN()
    {
        return $this->n;
    } 
    
     /**
     * Set n
     *
     * @param string $n
     *
     * @return Traspasos
     */
    public function setN($n)
    {
        $this->n = $n;

        return $this;
    }

    /**
     * Get op_id
     *
     * @return string
     */
    public function getOpId()
    {
        return $this->op_id;
    } 
    
     /**
     * Set op_id
     *
     * @param string $op_id
     *
     * @return Transfers
     */
    public function setOpId($op_id)
    {
        $this->op_id = $op_id;

        return $this;
    }  

    /**
     * Set obs
     *
     * @param string $obs
     *
     * @return Tos
     */
    public function setObs($obs)
    {
        $this->obs = $obs;

        return $this;
    }

    /**
     * Get obs
     *
     * @return string
     */
    public function getObs()
    {
        return $this->obs;
    } 

    /**
     * Set traspasotour_id
     *
     * @param integer $traspasotour_id
     *
     * @return TransferServicios
     */
    public function setServicioId($servicio_id)
    {
        $this->servicio_id = $servicio_id;

        return $this;
    }

    /**
     * Get servicio_id
     *
     * @return integer
     */
    public function getServicioId()
    {
        return $this->servicio_id;
    }   
    
    /**
     * Set hora_ingreso
     *
     * @param time $hora_ingreso
     *
     * @return time
     */
    public function setHoraingreso($hora_ingreso)
    {
        $this->hora_ingreso = $hora_ingreso;

        return $this;
    }

    /**
     * Get hora_ingreso
     *
     * @return time
     */
    public function getHoraingreso()
    {
        return $this->hora_ingreso;
    }       

    /**
     * Set usuario
     *
     * @param string $usuario
     *
     * @return Traspasos
     */
    
    
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string
     */
    public function getUsuario()
    {
        return $this->usuario;
    }   
    
    
    /**
     * Get nula
     *
     * @return integer
     */
    public function getNula()
    {
    	return $this->nula;
    }

    /**
     * Set nula
     *
     * @param integer $nula
     * @return integer
     */
    
    public function setNula($nula)
    {
    	$this->nula = $nula;
    
    	return $this;
    }  
              
    /**
     * Get hab
     *
     * @return integer
     */
    public function getHab()
    {
    	return $this->hab;
    }

    /**
     * Set hab
     *
     * @param integer $hab
     * @return integer
     */
    
    public function setHab($hab)
    {
    	$this->hab = $hab;
    
    	return $this;
    }  
 
      /**
     * Get hora_transfer
     *
     * @return time
     */
    public function getHoraTransfer()
    {
    	return $this->hora_transfer;
    }

    /**
     * Set hora_transfer
     *
     * @param time $hora_transfer
     * @return time
     */
    
    public function setHoraTransfer($hora_transfer)
    {
    	$this->hora_transfer = $hora_transfer;
    
    	return $this;
    }    
      /**
     * Get vuelo
     *
     * @return vuelo
     */

      /**
     * Get hora_vuelo
     *
     * @return time
     */
    public function getHoraVuelo()
    {
    	return $this->hora_vuelo;
    }

    /**
     * Set hora_vuelo
     *
     * @param time $hora_vuelo
     * @return time
     */
    
    public function setHoraVuelo($hora_vuelo)
    {
    	$this->hora_vuelo = $hora_vuelo;
    
    	return $this;
    }



    public function getVuelo()
    {
    	return $this->vuelo;
    }

    /**
     * Set vuelo
     *
     * @param string $vuelo
     * @return string
     */
    
    public function setVuelo($vuelo)
    {
    	$this->vuelo = $vuelo;
    
    	return $this;
    }  
    /**
     * @ORM\OneToMany(targetEntity="Tos", mappedBy="traspaso")
     * @ORM\OneToMany(targetEntity="TraspasosTours", mappedBy="traspaso")
     */
    private $traspasos;
    
    public function __construct()
        
    {
        $this->traspasos = new ArrayCollection();
    }    
    
    
}


