<?php

namespace Gestion\TraspasosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * traspasos_precios
 *
 * @ORM\Table(name="traspasos_precios", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Gestion\TraspasosBundle\Entity\TraspasosPreciosRepository")
 */

class TraspasosPrecios
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     * 
     * @ORM\Column(name="to_id", type="integer", nullable=false)
     */
    private $to_id;
            
    /**
     * @var integer
     * 
     * @ORM\Column(name="tour_id", type="integer", nullable=false)
     */
    private $tour_id;
    
    
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="precio", type="integer", nullable=false)
     */
    private $precio;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set precio
     *
     * @param integer $precio
     *
     * @return TraspasosPrecios
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return integer
     */
    public function getPrecio()
    {
        return $this->precio;
    }
}

