<?php

namespace Gestion\MainBundle\Entity;
namespace Gestion\CajaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Util\Debug;

/**
 * monedas
 *
 * @ORM\Table(name="monedas", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 */

class Moneda
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")

     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="moneda", type="string", length=50, nullable=false)

     */
    private $moneda;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set moneda
     *
     * @param string $moneda
     * @return string
     */
    public function setMoneda($moneda)
    {
        $this->moneda = $moneda;

        return $this;
    }

    /**
     * Get moneda
     *
     * @return string 
     */
    public function getMoneda()
    {
        return $this->moneda;
    }
    
    /**
    * @ORM\ManyToOne(targetEntity="Caja", inversedBy="cajas2")  
    * @ORM\JoinColumn(name="moneda_id", referencedColumnName="id")
    * @ORM\ManyToOne(targetEntity="CajaCierre", inversedBy="cajascierre")  
    * @ORM\JoinColumn(name="moneda_id", referencedColumnName="id")
    *     */
    protected $cajacierre;
    
    
 public function __toString()
    {
        return strval($this->id);
    }
    
}