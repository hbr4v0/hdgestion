<?php

namespace Gestion\CajaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * caja_cierre
 *
 * @ORM\Table(name="caja_cierre", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Gestion\CajaBundle\Entity\CajaCierreRepository")
 */
class CajaCierre
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var date
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

     /**
     * @var time
     *
     * @ORM\Column(name="hora", type="time", nullable=false)
     */
    private $hora;

    /**
     * @var date
     *
     * @ORM\Column(name="cierre", type="date", nullable=true)
     */
    private $cierre;

    /**
     * @var integer
     *
     * @ORM\Column(name="saldo", type="integer", nullable=false)
     */
    private $saldo;
   
    /**
     * @var string
     *
     * @ORM\Column(name="obs", type="string", length=255, nullable=true)
     */
    private $obs;

   
    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=50, nullable=true)
     */
    private $usuario;    

     /**
     * @var integer
     *
     * @ORM\Column(name="moneda_id", type="integer", nullable=false)
     */
    private $moneda_id;       
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param date $fecha
     * @return date
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }    
        
    /**
     * Set cierre
     *
     * @param date $cierre
     * @return date
     */
    public function setCierre($cierre)
    {
        $this->cierre = $cierre;

        return $this;
    }

    /**
     * Get cierre
     *
     * @return date
     */
    public function getCierre()
    {
        return $this->cierre;
    }
    
    /**
     * Set saldo
     *
     * @param integer $saldo
     * @return integer
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;

        return $this;
    }

    /**
     * Get saldo
     *
     * @return integer 
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * Set hora
     *
     * @param time $hora
     * @return time
     */
    public function setHora($hora)
    {
        $this->hora = $hora;

        return $this;
    }
    
    /**
     * Set usuario
     *
     * @param string $usuario
     * @return string
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }    
    
    /**
     * Get moneda_id
     *
     * @return integer
     */
    public function getMonedaId()
    {
    	return $this->moneda_id;
    }

    /**
     * Set moneda_id
     *
     * @param integer $moneda_id
     * @return integer
     */
    
    public function setMonedaId($moneda_id)
    {
    	$this->moneda_id = $moneda_id;
    
    	return $this;
    }       
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="Moneda", mappedBy="cajacierre")
     */
    private $cajascierre;
    
    public function __construct()
        
    {
        $this->cajascierre = new ArrayCollection();
    }
    
    
    
}

