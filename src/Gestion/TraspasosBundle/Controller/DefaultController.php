<?php

namespace Gestion\TraspasosBundle\Controller;

use Gestion\TraspasosBundle\Entity\Traspasos;
use Gestion\TraspasosBundle\Entity\Tos;
use Gestion\TraspasosBundle\Entity\TraspasosTours;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ob\HighchartsBundle\Highcharts\Highchart;

class DefaultController extends Controller
{
    public function indexAction()   
    {
        $hoy = new \DateTime("now");
        $ayer = new \DateTime("now");
        $ayer->modify('-1 day');
        $m = new \DateTime("now");
        $m->modify('+1 day');
        $manana = $m->format("Y-m-d");
        $m->modify('+1 day');
        $pasado = $m->format("Y-m-d");
        
        
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
        'SELECT t.id, t.fecha_traspaso, t.costo, t.hab, t.pax, t.n, t.cobrado, t.nula, t.pagado, o.operador, p.tour, t.obs 
         FROM GestionTraspasosBundle:Traspasos t 
         inner JOIN GestionTraspasosBundle:Tos o
         with t.to_id = o.id
         inner JOIN GestionTraspasosBundle:TraspasosTours p
         with t.traspasotour_id = p.id
         WHERE (t.fecha_traspaso > :hoy) OR (t.pagado=0 AND t.nula=0) 
         ORDER BY t.pax, t.fecha_traspaso ASC'
        )->setParameter('hoy', $ayer);
        $movs = $query->getResult();       
        
        $hoy2 = date("Y-m-d");
        $query2 = $em->createQuery(
        'SELECT t.id, t.fecha_traspaso, t.costo, t.hab, t.pax, t.n, t.cobrado, t.nula, t.pagado, o.operador, p.tour 
         FROM GestionTraspasosBundle:Traspasos t 
         inner JOIN GestionTraspasosBundle:Tos o
         with t.to_id = o.id
         inner JOIN GestionTraspasosBundle:TraspasosTours p
         with t.traspasotour_id = p.id
         WHERE (t.fecha_traspaso = :hoy) AND (t.nula=0) 
         ORDER BY t.id DESC'
        )->setParameter('hoy', $hoy2);
        $tour_movs_hoy = $query2->getResult();        
        
        $query3 = $em->createQuery(
        'SELECT t.id, t.fecha_traspaso, t.costo, t.hab, t.pax, t.n, t.cobrado, t.nula, t.pagado, o.operador, p.tour  
         FROM GestionTraspasosBundle:Traspasos t 
         inner JOIN GestionTraspasosBundle:Tos o
         with t.to_id = o.id
         inner JOIN GestionTraspasosBundle:TraspasosTours p
         with t.traspasotour_id = p.id
         WHERE (t.fecha_traspaso = :manana) AND (t.nula=0) 
         ORDER BY t.id DESC'
        )->setParameter('manana', $manana);
        $tour_movs_manana = $query3->getResult();

        $query4 = $em->createQuery(
        'SELECT t.id, t.fecha_traspaso, t.costo, t.hab, t.pax, t.n, t.cobrado, t.nula, t.pagado, o.operador, p.tour  
         FROM GestionTraspasosBundle:Traspasos t 
         inner JOIN GestionTraspasosBundle:Tos o
         with t.to_id = o.id
         inner JOIN GestionTraspasosBundle:TraspasosTours p
         with t.traspasotour_id = p.id
         WHERE (t.fecha_traspaso = :manana) AND (t.nula=0) 
         ORDER BY t.id DESC'
        )->setParameter('manana', $pasado);
        $tour_movs_pasado = $query4->getResult();
        
        //RESUMEN MENSUAL
        $mes=date("m");
        $anio=date("Y");
        $dias_mes= date("d",(mktime(0,0,0,$mes+1,1,$anio)-1));
        $dias=intval($dias_mes);
        $hasta= $anio . "-" . $mes . "-" . $dias ;
        $desde= date('Y-m-01'); // first day of this month

        // RESUMEN QUINCENAL VENTAS TOTALES 6 MESES
        
        //Recupera TOs y los mete a arreglo
        $query = $em->createQuery(
        "SELECT o.operador, SUM(t.costo) AS costo, SUM(t.cobrado) AS cobrado   
         FROM GestionTraspasosBundle:Traspasos t 
         inner JOIN GestionTraspasosBundle:Tos o
         with t.to_id = o.id
         WHERE (t.fecha_traspaso BETWEEN  '" . $desde . "' AND '" . $hasta . "') AND t.nula=0  
         GROUP BY o.operador");
        $ret = $query->getResult();
        
        $qq=0;
                
        for ($i = 0; $i <= 11; $i++)
            {
                if ( $i > 0 ){
                $m->modify('-1 month');
                }
                
                $mes=$m->format("M") . "/" . $m->format("y");
                $nmes=$m->format("m");
                
                $fecha1_q1 = $m->format('Y') . "-" . $m->format('m') . "-01";
                $fecha2_q1 = $m->format('Y') . "-" . $m->format('m') . "-15";
                
                $fecha1_q2 = $m->format('Y') . "-" . $m->format('m') . "-16";

                $anio=date("Y");
                $dias_mes= date("d",(mktime(0,0,0,$nmes+1,1,$anio)-1));
                $dias=intval($dias_mes);
                
                $fecha2_q2 = $m->format('Y') . "-" . $m->format('m') . "-" . $dias;
                
                
                $q=$em->createQuery("SELECT Sum(t.costo) AS costo, Sum(t.cobrado) AS cobrado
                                    FROM GestionTraspasosBundle:Traspasos t 
                                    WHERE t.fecha_traspaso Between '". $fecha1_q1 . "' And '" . $fecha2_q1 . "' AND t.nula=0"
                                    );
                $result=$q->getResult();
                $q1= array_shift($result['0']);
 
                $q=$em->createQuery("SELECT Sum(t.costo) AS costo, Sum(t.cobrado) AS cobrado
                                    FROM GestionTraspasosBundle:Traspasos t 
                                    WHERE t.fecha_traspaso Between '". $fecha1_q2 . "' And '" . $fecha2_q2 . "' AND t.nula=0"
                                    );
                $result=$q->getResult();
                $q2= array_shift($result['0']);
                
                $q1=intval($q1);
                $q2=intval($q2);
                
                $qq=$q1+$q2;

                $ret[$i] = array('mes'=>$mes, 'q1'=>$q1, 'q2'=>$q2, 'total'=>$qq);
            }

            // % Variacion
            
            $tot1 = $ret[0]['total'];
            $tot2 = $ret[1]['total'];
            $tot3 = $ret[2]['total'];
            $tot4 = $ret[3]['total'];
            $tot5 = $ret[4]['total'];
            $tot6 = $ret[5]['total'];

            
            if ($tot2 == '0'){
                $var1 = 100;
            }
            else {
                $var1 = (100-(($tot1*100)/$tot2))*-1;
            }
 
            if ($tot3 == '0'){
                $var2 = 0;
            }
            else {
                $var2 = (100-(($tot2*100)/$tot3))*-1;
            }
            
            if ($tot4 == '0'){
                $var3 = 0;
            }
            else {
                $var3 = (100-(($tot3*100)/$tot4))*-1;
            }
            
            if ($tot5 == '0'){
                $var4 = 0;
            }
            else {
                $var4 = (100-(($tot4*100)/$tot5))*-1;
            }           

            if ($tot6 == '0'){
                $var5 = 0;
            }
            else {
                $var5 = (100-(($tot5*100)/$tot6))*-1;
            }   
            
            $var6=0; 
            
            $ret[0]['variacion']=$var1;
            $ret[1]['variacion']=$var2;
            $ret[2]['variacion']=$var3;
            $ret[3]['variacion']=$var4;
            $ret[4]['variacion']=$var5;     
            $ret[5]['variacion']=$var6;     
            
        $tour_historico_vtas=$ret;
        unset($tour_historico_vtas[6]);
        unset($tour_historico_vtas[7]);
        unset($tour_historico_vtas[8]);
        unset($tour_historico_vtas[9]);
        unset($tour_historico_vtas[10]);
        unset($tour_historico_vtas[11]);
        
        $gra_historico_vtas_data1= array_column($ret, 'q1') ;
        $gra_historico_vtas_data2= array_column($ret, 'q2') ;
        $gra_historico_vtas_data3= array_column($ret, 'total') ;
        $gra_historico_vtas_cat= array_column($ret, 'mes') ;
        
        
        $query4 = $em->createQuery(
        "SELECT t.id, t.pax, t.n, t.fecha_pago as fecha,t.hora_pago, t.formapago, t.cobrado  
         FROM GestionTraspasosBundle:Traspasos t 
         WHERE t.pagado = '1'
         ORDER BY t.fecha_pago DESC
         ")->setMaxResults(6);
        $ultimos_pagos = $query4->getResult();  
        
        $query5 = $em->createQuery(
        "SELECT SUM(t.cobrado) AS cobrado  
         FROM GestionTraspasosBundle:Traspasos t 
         WHERE t.pagado = '0'
         ");
        $tour_total_impagos = $query5->getResult(); 
        $tour_total_impagos = array_shift($tour_total_impagos['0']);

        
        //INFO TRANSFERS
        $query5 = $em->createQuery(
        'SELECT t.id, t.fecha_transfer,t.vuelo,   t.hora_transfer, t.hab, t.pax, t.n, t.nula, o.operador, p.servicio  
         FROM GestionTraspasosBundle:Transfers t 
         inner JOIN GestionTraspasosBundle:TransferOps o
         with t.op_id = o.id
         inner JOIN GestionTraspasosBundle:TransferServicios p
         with t.servicio_id = p.id
         WHERE (t.fecha_transfer > :hoy) OR (t.nula=0) 
         ORDER BY t.pax, t.fecha_transfer ASC'
        )->setParameter('hoy', $ayer);
        $trnf_movs = $query5->getResult();       
        
        
        $hoy2 = date("Y-m-d");
        $query6 = $em->createQuery(
        'SELECT t.id, t.fecha_transfer,t.vuelo,   t.hora_transfer, t.hab, t.pax, t.n, t.nula, o.operador, p.servicio  
         FROM GestionTraspasosBundle:Transfers t 
         inner JOIN GestionTraspasosBundle:TransferOps o
         with t.op_id = o.id
         inner JOIN GestionTraspasosBundle:TransferServicios p
         with t.servicio_id = p.id
         WHERE (t.fecha_transfer =:hoy) AND (t.nula=0) 
         ORDER BY t.pax, t.fecha_transfer ASC'
        )->setParameter('hoy', $hoy2);
        $trnf_movs_hoy = $query6->getResult();   
        
        $query7 = $em->createQuery(
        'SELECT t.id, t.fecha_transfer,t.vuelo,   t.hora_transfer, t.hab, t.pax, t.n, t.nula, o.operador, p.servicio  
         FROM GestionTraspasosBundle:Transfers t 
         inner JOIN GestionTraspasosBundle:TransferOps o
         with t.op_id = o.id
         inner JOIN GestionTraspasosBundle:TransferServicios p
         with t.servicio_id = p.id
         WHERE (t.fecha_transfer =:manana) AND (t.nula=0) 
         ORDER BY t.pax, t.fecha_transfer ASC'
        )->setParameter('manana', $manana);
        $trnf_movs_manana = $query7->getResult();

        $query8 = $em->createQuery(
        'SELECT t.id, t.fecha_transfer,t.vuelo,   t.hora_transfer, t.hab, t.pax, t.n, t.nula, o.operador, p.servicio  
         FROM GestionTraspasosBundle:Transfers t 
         inner JOIN GestionTraspasosBundle:TransferOps o
         with t.op_id = o.id
         inner JOIN GestionTraspasosBundle:TransferServicios p
         with t.servicio_id = p.id
         WHERE (t.fecha_transfer =:pasado) AND (t.nula=0) 
         ORDER BY t.pax, t.fecha_transfer ASC'
        )->setParameter('pasado', $pasado);
        $trnf_movs_pasado = $query8->getResult();
        
        //RESUMEN MENSUAL
        $mes=date("m");
        $anio=date("Y");
        $dias_mes= date("d",(mktime(0,0,0,$mes+1,1,$anio)-1));
        $dias=intval($dias_mes);
        $hasta= $anio . "-" . $mes . "-" . $dias ;
        $desde= date('Y-m-01'); // first day of this month
        
        $query8 = $em->createQuery(
        "SELECT o.operador, p.servicio, Sum(t.n) AS N
         FROM GestionTraspasosBundle:Transfers t 
         inner JOIN GestionTraspasosBundle:TransferOps o
         with t.op_id = o.id
         inner JOIN GestionTraspasosBundle:TransferServicios p
         with t.servicio_id = p.id
         WHERE t.fecha_transfer Between '" . $desde . "' AND '" . $hasta . "' AND t.nula=0
         GROUP BY o.operador, p.servicio, t.nula
         HAVING t.nula=0
         ORDER BY p.servicio");
        $trnf_resumen_mes = $query8->getResult();  

        // FIN INFO TRANSFER
        
        return $this->render
                ('GestionTraspasosBundle:Default:index.html.twig', 
                array(
                    'movs'=>$movs,
                    'tour_movs_hoy'=>$tour_movs_hoy,
                    'hoy'=>$hoy,
                    'manana'=>$manana,
                    'pasado'=>$pasado,
                    'tour_movs_manana'=>$tour_movs_manana,
                    'tour_movs_pasado'=>$tour_movs_pasado,
                    'tour_total_impagos'=>$tour_total_impagos,
                    'tour_historico_vtas'=>$tour_historico_vtas,
                    'trnf_movs'=>$trnf_movs,
                    'trnf_movs_hoy'=>$trnf_movs_hoy,
                    'trnf_movs_manana'=>$trnf_movs_manana,
                    'trnf_movs_pasado'=>$trnf_movs_pasado,
                    'trnf_resumen_mes'=>$trnf_resumen_mes,
                    'ultimos_pagos'=>$ultimos_pagos
                    )
                    );
    }

        public function indextoursAction()   
    {
        
        $hoy = new \DateTime("now");
        $ayer = new \DateTime("now");
        $ayer->modify('-1 day');
        $m = new \DateTime("now");
        $m->modify('+1 day');
        $manana = $m->format("Y-m-d");
        $m->modify('+1 day');
        $pasado = $m->format("Y-m-d");
        
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
        'SELECT t.id, t.fecha_traspaso,   t.costo, t.hab, t.pax, t.n, t.cobrado, t.nula, t.pagado, o.operador, p.tour, t.obs   
         FROM GestionTraspasosBundle:Traspasos t 
         inner JOIN GestionTraspasosBundle:Tos o
         with t.to_id = o.id
         inner JOIN GestionTraspasosBundle:TraspasosTours p
         with t.traspasotour_id = p.id
         WHERE (t.fecha_traspaso > :hoy) OR (t.pagado=0 AND t.nula=0) 
         ORDER BY t.fecha_traspaso, t.pax DESC'
        )->setParameter('hoy', $ayer);

        $movs = $query->getResult();       
        
        $hoy2 = date("Y-m-d");
        $query2 = $em->createQuery(
        'SELECT t.id, t.fecha_traspaso, t.costo, t.hab, t.pax, t.n, t.cobrado, t.nula, t.pagado, o.operador, p.tour  
         FROM GestionTraspasosBundle:Traspasos t 
         inner JOIN GestionTraspasosBundle:Tos o
         with t.to_id = o.id
         inner JOIN GestionTraspasosBundle:TraspasosTours p
         with t.traspasotour_id = p.id
         WHERE (t.fecha_traspaso = :hoy) AND (t.nula=0) 
         ORDER BY t.id DESC'
        )->setParameter('hoy', $hoy2);

        $movs_hoy = $query2->getResult();        
        
        $query3 = $em->createQuery(
        'SELECT t.id, t.fecha_traspaso, t.costo, t.hab, t.pax, t.n, t.cobrado, t.nula, t.pagado, o.operador, p.tour  
         FROM GestionTraspasosBundle:Traspasos t 
         inner JOIN GestionTraspasosBundle:Tos o
         with t.to_id = o.id
         inner JOIN GestionTraspasosBundle:TraspasosTours p
         with t.traspasotour_id = p.id
         WHERE (t.fecha_traspaso = :manana) AND (t.nula=0) 
         ORDER BY t.id DESC'
        )->setParameter('manana', $manana);

        $movs_manana = $query3->getResult();

        $query4 = $em->createQuery(
        'SELECT t.id, t.fecha_traspaso, t.costo, t.hab, t.pax, t.n, t.cobrado, t.nula, t.pagado, o.operador, p.tour  
         FROM GestionTraspasosBundle:Traspasos t 
         inner JOIN GestionTraspasosBundle:Tos o
         with t.to_id = o.id
         inner JOIN GestionTraspasosBundle:TraspasosTours p
         with t.traspasotour_id = p.id
         WHERE (t.fecha_traspaso = :pasado) AND (t.nula=0) 
         ORDER BY t.id DESC'
        )->setParameter('pasado', $pasado);

        $movs_pasado = $query4->getResult();
        
        //ULTIMOS PAGOS
        $mes=date("m");
        $anio=date("Y");
        $dias_mes= date("d",(mktime(0,0,0,$mes+1,1,$anio)-1));
        $dias=intval($dias_mes);
        $hasta= $anio . "-" . $mes . "-" . $dias ;
        $desde= date('Y-m-01'); // first day of this month
        
        $query4 = $em->createQuery(
        "SELECT t.id, t.pax, t.n, t.fecha_pago as fecha,t.hora_pago, t.formapago, t.cobrado  
         FROM GestionTraspasosBundle:Traspasos t 
         WHERE t.pagado = '1'
         ORDER BY t.fecha_pago DESC
         ")->setMaxResults(10);
        $ultimos_pagos = $query4->getResult();  

        return $this->render
                ('GestionTraspasosBundle:Default:index.tours.html.twig', 
                array(
                    'movs'=>$movs,
                    'movs_hoy'=>$movs_hoy,
                    'hoy'=>$hoy,
                    'manana'=>$manana,
                    'pasado'=>$pasado,
                    'ultimos_pagos'=>$ultimos_pagos,
                    'movs_manana'=>$movs_manana,
                    'movs_pasado'=>$movs_pasado)
                    );
    }

    public function addtourAction(Request $request)
            
    {
            $traspaso = new Traspasos();
            
            $form = $this->createFormBuilder($traspaso)
            ->add('fecha_traspaso', 'genemu_jquerydate',array('widget'  => 'single_text',
                      'attr' => array('class' => 'form-control','format'   => 'y-MM-dd', 
                      )))
            ->add('pax', 'text',array('attr' => array('class' => 'form-control', )  ))
            ->add('n','integer',array( 'attr' => array('class' => 'form-control','placeholder' => '0','required' => 'true')))
            ->add('cobrado','integer',array( 'attr' => array('class' => 'form-control','placeholder' => '0','required' => 'true')))
            ->add('to_id', 'genemu_jqueryselect2_entity', array(
                    'class' => 'GestionTraspasosBundle:Tos',
                    'property' => 'operador',
                'attr' => array('class' => 'form-control'),
                'required' => 'true',
                'empty_value'=>'Seleccione...',))                            
            ->add('traspasotour_id', 'genemu_jqueryselect2_entity', array(
                    'class' => 'GestionTraspasosBundle:TraspasosTours',
                    'property' => 'tour',
                'attr' => array('class' => 'form-control'),
                'required' => 'true',
                'empty_value'=>'Seleccione...',))  
            ->add('obs','text',array( 'attr' => array('class' => 'form-control','required' => false,'empty_value'=>'null')))
            ->add('hab', 'genemu_jqueryselect2_choice', array(
                            'choices' => array(
                                            '0' => '0',
                                            '1' => '1',
                                            '2' => '2',
                                            '3' => '3',
                                            '4' => '4',
                                            '5' => '5',
                                            '6' => '6',
                                            '7' => '7',
                                            '8' => '8',
                                            '9' => '9'),
                            'empty_value'=>'Seleccione...',
                            'attr' => array('class' => 'form-control')))            
            ->add('Ingresar','submit',array( 'attr' => array('class' => 'btn btn-info')))
                    
            ->getform();

            $form->handleRequest($request);

            if ($form->isValid()) {
                
                //Calcula campo costo
                $em = $this->getDoctrine()->getManager();
                $to_id=$form["to_id"]->getData();;
                $tour_id=$form["traspasotour_id"]->getData();    
                $precio = $em->getRepository('GestionTraspasosBundle:TraspasosPrecios')
                         ->precio($to_id,$tour_id);
                $precio = array_shift($precio['0']);
                $n = $form["n"]->getData();
                $costo= $n * $precio;
                
                //Capitaliza nombre
                $pax=$form["pax"]->getData(); 
                $cpax = ucwords(strtolower($pax));
                $traspaso->setPax($cpax);
                
                $fecha_traspaso=$form["fecha_traspaso"]->getData();
               // $ffecha_traspaso = \DateTime::createFromFormat('Y-m-d', $fecha_traspaso);
                $traspaso->setFechaTraspaso($fecha_traspaso);
                $traspaso->setFechaIngreso(new \DateTime("now"));    
                $traspaso->setHoraingreso(new \DateTime("now"));                
                $traspaso->setUsuario($this->getUser());
                $traspaso->setCosto($costo);
                $traspaso->setPagado('0');
                //$traspaso->setPagoDoc('0');
                $traspaso->setNula(0);
                
                $em= $this->getDoctrine()->getManager();
                $em->persist($traspaso);
                $em->flush();

                return $this->redirect($this->generateUrl('gestion_traspasos_tours'));
            }

            return $this->render
                    ('GestionTraspasosBundle:Default:traspasos.addtour.html.twig', 
                    array("form"=>$form->createview(),
                        )
                    );
    }        

    public function editartourAction($id,Request $request)
            
    {
        $em = $this->getDoctrine()->getEntityManager();
        $traspaso = $em->getRepository('GestionTraspasosBundle:Traspasos')->find($id);

        if (!$traspaso) {
            throw $this->createNotFoundException('Registro inexistente...');
        }
        
        $to_id = $traspaso->getToId();
        $to = $em->getRepository('GestionTraspasosBundle:Tos')->find($to_id);
        $operador=$to->getOperador();            

        $tour_id = $traspaso->getTraspasoTourId();
        $t = $em->getRepository('GestionTraspasosBundle:TraspasosTours')->find($tour_id);
        $tour=$t->getTour(); 
        
        $form = $this->createFormBuilder($traspaso)
        ->add('fecha_traspaso', 'genemu_jquerydate',array('widget'  => 'single_text',
                  'attr' => array('class' => 'form-control','format'   => 'y-MM-dd', 
                  )))
        ->add('pax', 'text',array('attr' => array('class' => 'form-control', )  ))
        ->add('n','integer',array( 'attr' => array('class' => 'form-control','placeholder' => '0','required' => 'true')))
        ->add('cobrado','integer',array( 'attr' => array('class' => 'form-control','placeholder' => '0','required' => 'true')))
        
        //->add('to_id', 'genemu_jqueryselect2_entity', array(
        //      'class' => 'GestionTraspasosBundle:Tos',
        //      'property' => 'operador',
        //      'attr' => array('class' => 'form-control'),
        //      'required' => 'true',
        //      'empty_value'=>'Seleccione...',))  
                
        //->add('traspasotour_id', 'genemu_jqueryselect2_entity', array(
        //        'class' => 'GestionTraspasosBundle:TraspasosTours',
        //        'property' => 'tour',
        //    'attr' => array('class' => 'form-control'),
        //    'required' => 'true',
        //    'empty_value'=>'Seleccione...',))  
                
        ->add('obs','text',array( 'attr' => array('class' => 'form-control','required' => false,'empty_value'=>'null')))
        ->add('hab', 'genemu_jqueryselect2_choice', array(
                        'choices' => array(
                                        '0' => '0',
                                        '1' => '1',
                                        '2' => '2',
                                        '3' => '3',
                                        '4' => '4',
                                        '5' => '5',
                                        '6' => '6',
                                        '7' => '7',
                                        '8' => '8',
                                        '9' => '9'),
                        'empty_value'=>'Seleccione...',
                        'attr' => array('class' => 'form-control')))            
        ->add('Guardar','submit',array( 'attr' => array('class' => 'btn btn-info')))

        ->getform();

        $form->handleRequest($request);

        if ($form->isValid()) {
            //Calcula campo costo
            $em = $this->getDoctrine()->getManager();
            $to_id = $traspaso->getToId();
            $tour_id = $traspaso->getTraspasoTourId();
            $precio = $em->getRepository('GestionTraspasosBundle:TraspasosPrecios')
                     ->precio($to_id,$tour_id);
            $precio = array_shift($precio['0']);
            $n = $form["n"]->getData();
            $costo= $n * $precio;
            $traspaso->setCosto($costo);
            
            //Capitaliza nombre
            $pax=$form["pax"]->getData(); 
            $cpax = ucwords(strtolower($pax));
            $traspaso->setPax($cpax);

            $em->flush();
            return $this->redirect($this->generateUrl('gestion_traspasos_tours'));
        }

        return $this->render
                ('GestionTraspasosBundle:Default:traspasos.edittour.html.twig', 
                array("form"=>$form->createview(),
                      "operador"=>$operador,
                      "tour"=>$tour,
                    )
                );
    }        
    
    public function anulartraspasoAction($id)
	{
            $em = $this->getDoctrine()->getEntityManager();
            $traspaso = $em->getRepository('GestionTraspasosBundle:Traspasos')->find($id);
            $traspaso->setNula(1);
            $em->persist($traspaso);
            $flush = $em->flush();
            if ($flush == null) {
                    echo "Traspaso anulado correctamente";
            } else {
                    echo "El traspaso no se ha anulado";
            }
            return $this->redirect($this->generateUrl('gestion_traspasos_tours'));
	}

    public function habilitartraspasoAction($id)
	{
            $em = $this->getDoctrine()->getEntityManager();
            $traspaso = $em->getRepository('GestionTraspasosBundle:Traspasos')->find($id);
            $traspaso->setNula(0);
            $em->persist($traspaso);
            $flush = $em->flush();
            if ($flush == null) {
                    echo "Traspaso habilitada correctamente";
            } else {
                    echo "El traspaso no se ha habilitar";
            }
            return $this->redirect($this->generateUrl('gestion_traspasos_tours'));
	} 
    
    public function vertraspasoAction($id)
	{

            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
            'SELECT t.id, t.fecha_traspaso, t.fecha_ingreso, t.hora_ingreso, t.costo, t.hab,t.usuario, t.pax, t.n, t.cobrado, t.nula, t.pagado, o.operador, p.tour, t.obs, t.fecha_pago, t.hora_pago, t.formapago, t.pago_doc, t.user_pago    
             FROM GestionTraspasosBundle:Traspasos t 
             inner JOIN GestionTraspasosBundle:Tos o
             with t.to_id = o.id
             inner JOIN GestionTraspasosBundle:TraspasosTours p
             with t.traspasotour_id = p.id
             WHERE t.id = :id
             ORDER BY t.id DESC'
            )->setParameter('id', $id);

            $mov = $query->getResult();
            
            //var_dump($mov);
            
            return $this->render
                ('GestionTraspasosBundle:Default:traspaso.html.twig', 
                array('mov'=>$mov)
                );
	}
         
    
    
}
